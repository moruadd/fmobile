//
//  SceneDelegate.swift
//  FMobile
//
//  Created by Damir on 30.10.2023.
//

import UIKit
import Network
import Alamofire
import Fundamental

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    private let configurator = AppDelegateConfiguratorFactory.makeDefault()
    private var applicationCoordinator: ApplicationCoordinator?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        let rootController = CoordinatorNavigationController(backBarButtonImage: ImageType.backUi.image, closeBarButtonImage: ImageType.closeLine.image)
        let window = UIWindow(windowScene: windowScene)
        window.rootViewController = rootController
        window.makeKeyAndVisible()
        self.window = window
        
        let appSessionManager = AppDelegate.assembler.resolver.resolve(AppSessionManager.self)!
        let userSession = AppDelegate.assembler.resolver.resolve(UserSession.self)!
        
        if appSessionManager.isFirstLaunch {
            userSession.finish()
            appSessionManager.isFirstLaunch = false
        }
        
        setupNetwork()
        
        applicationCoordinator = ApplicationCoordinator(router: RouterImpl(rootController: rootController), container: AppDelegate.assembler.resolver)
        applicationCoordinator?.start()
    }
    
    func setupNetwork() {
        Network.shared.setHeaders(from: NetworkHeadersStorage())
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }
    
    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
}
