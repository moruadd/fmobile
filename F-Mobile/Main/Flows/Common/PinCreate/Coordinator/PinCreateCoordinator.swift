//
//  PinCreateCoordinator.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

//import Foundation
//import Fundamental
//
//protocol PinCreateCoordinatorOutput: AnyObject {
//    var onFlowDidFinish: Callback? { get set }
//}
//
//final class PinCreateCoordinator: Coordinator, PinCreateCoordinatorOutput {
//    var onFlowDidFinish: Callback?
//    var childCoordinators: [Coordinator] = []
//
//    private let router: Router
//    private let container: DependencyContainer
//    private let moduleFactory: PinCreateModuleFactory
//
//    init(router: Router, container: DependencyContainer) {
//        self.router = router
//        self.container = container
//        moduleFactory = PinCreateModuleFactory(container: container)
//    }
//
//    func start() {
//        showPinCreate()
//    }
//
//    private func showPinCreate() {
//        var pinCreate = moduleFactory.makePinCreate()
//        pinCreate.onPinCreateDidFinish = { [weak self] isPinCreated in
//            guard let self else { return }
//            if isPinCreated, self.container.resolve(BiometricAuthService.self)!.canAuthenticate() {
//                self.showBiometricAuthUsage()
//            } else {
//                self.onFlowDidFinish?()
//            }
//        }
//        router.setRootModule(pinCreate)
//    }
//
//    private func showBiometricAuthUsage() {
//        var biometricAuthUsage = moduleFactory.makeBiometricAuthUsage()
//        biometricAuthUsage.onBiometricAuthUsageDidFinish = { [weak self] in
//            self?.router.dismissModule(animated: true) { [weak self] in
//                self?.onFlowDidFinish?()
//            }
//        }
//        router.presentInSlidingCard(biometricAuthUsage, animated: true)
//    }
//}

