//
//  PinVerifyCoordinator.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import Foundation
import Fundamental

//protocol PinVerifyCoordinatorOutput: AnyObject {
//    var onFlowDidFinish: ((_ isVerified: Bool) -> Void)? { get set }
//}
//
//final class PinVerifyCoordinator: Coordinator, PinVerifyCoordinatorOutput {
//    var childCoordinators: [Coordinator] = []
//
//    var onFlowDidFinish: ((_ isVerified: Bool) -> Void)?
//
//    private let router: Router
//    private let moduleFactory: PinVerifyModuleFactory
//
//    init(router: Router, container: DependencyContainer) {
//        moduleFactory = PinVerifyModuleFactory(container: container)
//        self.router = router
//    }
//
//    func start() {
//        showPinVerify()
//    }
//
//    private func showPinVerify() {
//        var pinVerify = moduleFactory.makePinVerify()
//        pinVerify.onCloseDidTap = { [weak self] in
//            self?.onFlowDidFinish?(false)
//        }
//        pinVerify.onPinDidVerify = { [weak self] in
//            self?.onFlowDidFinish?(true)
//        }
//        pinVerify.onPinDidReject = { [weak self] in
//            self?.onFlowDidFinish?(false)
//        }
//        router.setRootModule(pinVerify)
//    }
//}

