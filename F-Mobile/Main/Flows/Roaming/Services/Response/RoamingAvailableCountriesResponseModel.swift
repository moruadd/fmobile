//
//  RoamingAvailableCountriesResponseModel.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import Foundation

struct RoamingAvailableCountriesResponseModel: Decodable {
    let availableCountries: [RoamingCountryResponseModel]
}

struct RoamingCountryResponseModel: Decodable, Equatable {
    let name: String
    let icon: String?
    let countryCode: String
}

