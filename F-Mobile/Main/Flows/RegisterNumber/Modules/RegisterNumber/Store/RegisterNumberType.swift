//
//  RegisterNumberType.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

enum RegisterNumberType {
    case checked(model: CheckedModel)
    case unchecked
    
    struct CheckedModel {
        let phoneNumber: String?
        let description: String?
        let buttonTitle: String?
    }
}

