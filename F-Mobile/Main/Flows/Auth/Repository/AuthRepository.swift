//
//  AuthRepository.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

//import Promises
//
//enum AuthError: Error {
//    case passwordNeedsChange
//    case notSubscriber
//    case corruptedStructure
//
//    var localizedDescription: String {
//        switch self {
//        case .passwordNeedsChange:
//            return AuthLocalization.passwordNeedsChange.localized
//        case .notSubscriber:
//            return AuthLocalization.notSubscriber.localized
//        case .corruptedStructure:
//            return AuthLocalization.corruptedStructure.localized
//        }
//    }
//}
//
//enum AuthPasswordRecoveryType {
//    case sms
//    case email
//}
//
//protocol SetNewPasswordRepository {
//    var username: String? { get set }
//    var usernameType: AuthUsernameType? { get set }
//
//    func setNewPassword(password: String) -> Promise<Void>
//    func authorize(with password: String) -> Promise<AuthUserType>
//}
//
//final class AuthRepository: OtpVerifyRepository, SetNewPasswordRepository {
//    typealias Provider = AuthProvider & AuthRecoveryEmailProvider & AuthRecoverySmsProvider
//
//    var username: String?
//    var usernameType: AuthUsernameType?
//    var authUserType: AuthUserType?
//    var passwordRecoveryType: AuthPasswordRecoveryType = .sms
//    var message: String?
//    var resendDelay: Int?
//    var hasEmail = false
//    var form: AuthClientStatus.Form?
//    var transitionType: AuthTransitionType?
//    var otpId: String? {
//        get {
//          userSession.otpId
//        }
//        set {
//          userSession.otpId = newValue
//        }
//      }
//
//    private let provider: Provider
//    private let userSession: UserSession
//
//    init(provider: Provider, userSession: UserSession) {
//        self.provider = provider
//        self.userSession = userSession
//    }
//
//    func authorize(with password: String) -> Promise<AuthUserType> {
//        guard let usernameType = usernameType,
//              let authUserType = authUserType,
//              let username = username else { return Promise(AuthError.corruptedStructure) }
//        switch (authUserType, usernameType) {
//        case (.regular, .phoneNumber):
//            return provider.postToken(msisdn: username, password: password)
//                .then { [weak self] response in
//                    self?.userSession.start(with: response, phoneNumber: username)
//                    return Promise(authUserType)
//                }
//        case (.regular, .email):
//            return provider.postToken(email: username, password: password)
//                .then { [weak self] response in
//                    self?.userSession.start(with: response, phoneNumber: nil)
//                    return Promise(authUserType)
//                }
//        case (.eshop, _):
//            return provider.postEshopToken(email: username, password: password)
//                .then { [weak self] response in
//                    self?.userSession.start(with: response, phoneNumber: nil)
//                    return Promise(authUserType)
//                }
//        }
//    }
//
//    func verify(username: String, usernameType: AuthUsernameType) -> Promise<AuthClientStatus> {
//        self.usernameType = usernameType
//
//        switch usernameType {
//        case .phoneNumber:
//            return provider.getClientStatus(msisdn: username)
//                .then { [weak self] status in
//                    guard let self = self else { throw VoidError() }
//                    self.username = username
//                    self.form = status.form
//                    self.hasEmail = status.hasEmail
//                    self.authUserType = .regular(usernameType: .phoneNumber)
//                    self.transitionType = AuthTransitionType(status: status)
//                }
//        case .email:
//            return provider.getClientStatus(email: username)
//                .then { [weak self] status in
//                    guard let self = self else { throw VoidError() }
//                    self.username = username
//                    self.form = status.form
//                    self.hasEmail = status.hasEmail
//                    self.transitionType = AuthTransitionType(status: status)
//                    if status.loginTransition == .goToEshop {
//                        self.authUserType = .eshop
//                    } else {
//                        self.authUserType = .regular(usernameType: .email)
//                    }
//                }
//        }
//    }
//
//    func send() -> Promise<Void> {
//        sendCode()
//    }
//
//    func resend() -> Promise<Void> {
//        sendCode()
//    }
//
//    private func sendCode() -> Promise<Void> {
//        guard let usernameType = usernameType,
//            let username = username
//        else {
//            return Promise(AuthError.corruptedStructure)
//        }
//        switch (passwordRecoveryType, usernameType) {
//        case (.sms, .phoneNumber):
//            return provider.postRecoverySmsSendOtp(msisdn: username)
//                .then(saveSendCodeResponse)
//                .voided()
//        case (.sms, .email):
//            return provider.postRecoverySmsSendOtp(email: username)
//                .then(saveSendCodeResponse)
//                .voided()
//        case (.email, .phoneNumber):
//            return provider.postRecoveryEmailSendEmail(msisdn: username)
//                .then(saveSendCodeResponse)
//                .voided()
//        case (.email, .email):
//            return provider.postRecoveryEmailSendEmail(email: username)
//                .then(saveSendCodeResponse)
//                .voided()
//        }
//    }
//
//    private func saveSendCodeResponse(_ response: SendCodeResponse) {
//        message = response.message
//        resendDelay = response.resendDelay
//        otpId = response.otpId
//    }
//
//    func verify(otp: String) -> Promise<Void> {
//        guard let usernameType = usernameType,
//            let username = username,
//            let otpId = otpId
//        else {
//            return Promise(AuthError.corruptedStructure)
//        }
//        switch (passwordRecoveryType, usernameType) {
//        case (.sms, .phoneNumber):
//            return provider.postRecoverySmsConfirmOtp(msisdn: username, otp: otp, otpId: otpId).voided()
//        case (.sms, .email):
//            return provider.postRecoverySmsConfirmOtp(email: username, otp: otp, otpId: otpId).voided()
//        case (.email, .phoneNumber):
//            return provider.postRecoveryEmailConfirmCode(msisdn: username, otp: otp, otpId: otpId).voided()
//        case (.email, .email):
//            return provider.postRecoveryEmailConfirmCode(email: username, otp: otp, otpId: otpId).voided()
//        }
//    }
//
//    func setNewPassword(password: String) -> Promise<Void> {
//        guard let usernameType = usernameType,
//            let username = username,
//            let otpId = otpId
//        else {
//            return Promise(AuthError.corruptedStructure)
//        }
//        switch (passwordRecoveryType, usernameType) {
//        case (.sms, .phoneNumber):
//            return provider.postRecoverySmsSetNewPassword(msisdn: username, password: password, otpId: otpId).voided()
//        case (.sms, .email):
//            return provider.postRecoverySmsSetNewPassword(email: username, password: password, otpId: otpId).voided()
//        case (.email, .phoneNumber):
//            return provider.postRecoveryEmailSetNewPassword(msisdn: username, password: password, otpId: otpId).voided()
//        case (.email, .email):
//            return provider.postRecoveryEmailSetNewPassword(email: username, password: password, otpId: otpId).voided()
//        }
//    }
//}
