//
//  AuthNavigationActionType.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

indirect enum AuthNavigationActionType {
    case changeInterfaceLanguage
    case showAuthPassword
    case showFaq
    case showOffices
    case showEshop
    case showMnp
    case showActionAlert(argument: AuthActionAlertArgument)
    case unknown
    case goToSetPassword
}

struct AuthActionAlertArgument {
    let title: String?
    let message: String?
    let rightButtonTitle: String?
    let action: AuthNavigationActionType
}

