//
//  AuthModuleFactory.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import Fundamental

typealias AuthModuleFactory = AnyModuleFactory<AuthCoordinator, AuthPresenter>

struct AuthModuleFactoryImpl {}

// MARK: - ModuleFactory

extension AuthModuleFactoryImpl: ModuleFactory {
    
    func createModule(withCoordinator coordinator: AuthCoordinator) -> Module<AuthPresenter> {
        let viewController = AuthViewController.instantiate(with: .main)
        viewController.presenter = AuthPresenterImpl()
        viewController.presenter.coordinator = coordinator
        viewController.presenter.view = viewController
        return Module(view: viewController, presenter: viewController.presenter)
    }
}

