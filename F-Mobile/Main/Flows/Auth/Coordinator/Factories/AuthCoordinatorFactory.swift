//
//  AuthCoordinatorFactory.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import Foundation
import Fundamental

//final class AuthCoordinatorFactory {
//    private let router: Router
//    private let container: DependencyContainer
////    private let repository: AuthRepository
//
//    init(router: Router, container: DependencyContainer) {
//        self.router = router
//        self.container = container
//        self.repository = repository
//    }
//
//    func makePinCreate() -> (coordinator: Coordinator & PinCreateCoordinatorOutput, module: Presentable?) {
//        let rootController = CoordinatorNavigationController(backBarButtonImage: ImageType.backUi.image)
//        let coordinator = PinCreateCoordinator(router: RouterImpl(rootController: rootController),
//                                               container: container)
//        return (coordinator, rootController)
//    }
//
//    func makePinVerify() -> (coordinator: Coordinator & PinVerifyCoordinatorOutput, module: Presentable?) {
//        let rootController = CoordinatorNavigationController(backBarButtonImage: ImageType.backUi.image)
//        let coordinator = PinVerifyCoordinator(router: RouterImpl(rootController: rootController), container: container)
//        return (coordinator, rootController)
//    }
//
////    func makeEshop() -> EshopCoordinator {
////        Eshop.startEshop()
////    }
////
////    func makeMNP() -> MNPCoordinator {
////        Eshop.startMNP()
////    }
//}

