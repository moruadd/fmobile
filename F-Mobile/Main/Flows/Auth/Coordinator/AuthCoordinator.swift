//
//  AuthCoordinator.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import UIKit
import Fundamental

protocol AuthCoordinatorOutput: AnyObject {
    var onFlowDidFinish: Callback? { get set }
    
    func start()
    func showPhoneEntryScreen()
    func showOtpScreen()
}

final class AuthCoordinator: BaseCoordinator, AuthCoordinatorOutput {
    var onFlowDidFinish: Callback?

    private let container: DependencyContainer
    private let moduleFactory: AuthModuleFactory
    private let signInModuleFactory: SignInModuleFactory
//    private let coordinatorFactory: AuthCoordinatorFactory
    private let application: UIApplication
    private let navigationControllerFlowDismissController: NavigationControllerFlowDismissController

    init(router: Router, container: DependencyContainer, navigationControllerFlowDismissController: NavigationControllerFlowDismissController, authModuleFactory: AuthModuleFactory, signInModuleFactory: SignInModuleFactory) {
        self.container = container
//        let repository = AuthRepository(provider: container.resolve(Provider.self)!, userSession: container.resolve(UserSession.self)!)
//        coordinatorFactory = AuthCoordinatorFactory(router: router, container: container, repository: repository)
        application = container.resolve(UIApplication.self)!
        self.navigationControllerFlowDismissController = navigationControllerFlowDismissController
        self.moduleFactory = authModuleFactory
        self.signInModuleFactory = signInModuleFactory
        super.init(router: router)
    }
    
    convenience init(router: Router, container: DependencyContainer) {
        self.init(router: router,
                  container: container,
                  navigationControllerFlowDismissController: NavigationControllerFlowDismissControllerImpl(),
                  authModuleFactory: AnyModuleFactory(AuthModuleFactoryImpl()),
                  signInModuleFactory: AnyModuleFactory(SignInModuleFactoryImpl())
        )
    }

    override func start() {
        showAuthUsername()
    }
    
    private func showAuthUsername() {
        let authModule = moduleFactory.createModule(withCoordinator: self)
        router.setRootModule(authModule.view, isNavigationBarHidden: false)
    }
    
    func showPhoneEntryScreen() {
        let signInModule = signInModuleFactory.createModule(withCoordinator: self)
        router.push(signInModule.view, animated: true, hideBottomBarWhenPushed: true, completion: nil)
    }
    
    func showOtpScreen() {
        
    }
}

// MARK: - EshopCoordinatorDelegate

//extension AuthCoordinator: EshopCoordinatorDelegate {
//
//    func eshopCoordinatorDidFinish(with coordinator: EshopCoordinator) {
//        coordinator.rootViewController.dismiss(animated: true)
//        removeCoordinator(coordinator)
//    }
//}
