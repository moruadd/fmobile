//
//  AuthViewController.swift
//  F-Mobile
//
//  Created by Damir on 16.10.2023.
//

import UIKit
import Components

final class AuthViewController: UIViewController {
    
    
    @IBOutlet private weak var signInButton: UIButton!
    @IBOutlet private weak var eshopButton: UIButton!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var welcomeImageView: UIImageView!
    
    var presenter: AuthPresenter!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        UIToken.configure(applicationType: .tele2)
    }
    
    @IBAction func eshopButtonWasPressed(_ sender: Any) {
        print("lol")
    }
    
    @IBAction func signInButtonPressed() {
        presenter.onSignInButtonClicked()
    }
}

extension AuthViewController: AuthView { }

private extension AuthViewController {
    
    func setupUI() {
        view.backgroundColor = .white
        welcomeImageView.backgroundColor = .gray
        titleLabel.text = "Welcome Screen"
        signInButton.setTitle("Войти", for: .normal)
        eshopButton.setTitle("Стать абонентом", for: .normal)
    }
}
