//
//  AuthPresenter.swift
//  F-Mobile
//
//  Created by Damir on 16.10.2023.
//

import Fundamental
import Alamofire
import Network
import UIKit

protocol AuthPresenter {
    var view: AuthView! { get set }
    var coordinator: AuthCoordinator! { get set }
    
    // MARK: - View Events
    
    func onViewDidLoad()
    func onSignInButtonClicked()
}

final class AuthPresenterImpl {
    
    // MARK: Public Properties
    
    weak var view: AuthView!
    weak var coordinator: AuthCoordinator!
    
    // MARK: - Private Properties
    
    // MARK: - Dependency
    
    // MARK: - Init
    
}

// MARK: - AuthPresenter

extension AuthPresenterImpl: AuthPresenter {
    
    func onViewDidLoad() {
        
    }
    
    func onSignInButtonClicked() {
        coordinator.showPhoneEntryScreen()
    }
}

// MARK: - Private

private extension AuthPresenterImpl {
    
}

