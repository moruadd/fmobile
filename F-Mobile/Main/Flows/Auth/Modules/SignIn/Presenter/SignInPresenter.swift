//
//  SignInPresenter.swift
//  F-Mobile
//
//  Created by Damir on 30.10.2023.
//

import Fundamental
import Alamofire
import Network
import UIKit

protocol SignInPresenter {
    var view: SignInView! { get set }
    var coordinator: AuthCoordinator! { get set }
    
    // MARK: - View Events
    
    func onViewDidLoad()
    func onActionButtonClicked(with phoneNumber: String)
}

final class SignInPresenterImpl {
    
    // MARK: Public Properties
    
    weak var view: SignInView!
    weak var coordinator: AuthCoordinator!
    
    // MARK: - Private Properties

    
    // MARK: - Dependency

    private let service: SignInService

    // MARK: - Init
    
    init(service: SignInService) {
        self.service = service
    }

    convenience init() {
        self.init(service: SignInServiceImpl())
    }
}

// MARK: - SignInPresenter

extension SignInPresenterImpl: SignInPresenter {
    
    func onViewDidLoad() {
        print("shocked")
    }
    
    func onActionButtonClicked(with phoneNumber: String) {
        service.getOtpCode(with: phoneNumber) { [weak self] result in
            guard let self else { return }
            
            switch result {
            case .success(let success):
                self.coordinator.showOtpScreen()
            case .failure(let failure):
                print(failure)
            }
        }
    }
}

// MARK: - Private

private extension SignInPresenterImpl {
    
   
}

