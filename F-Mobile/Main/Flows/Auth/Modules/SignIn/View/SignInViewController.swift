//
//  SignInViewController.swift
//  F-Mobile
//
//  Created by Damir on 30.10.2023.
//

import UIKit
import Components

final class SignInViewController: UIViewController {
    
    var presenter: SignInPresenter!
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var desciriptionLabel: UILabel!
    @IBOutlet private weak var phoneTextField: PhoneNumberTextField!
    @IBOutlet private weak var actionButton: Button!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        setupUI()
        hideKeyboardWhenTappedAround()
    }
    
    
    @IBAction func actionButtonWasPressed(_ sender: Any) {
        guard let phoneNumber = phoneTextField.getRawText() else { return }
        
        presenter.onActionButtonClicked(with: phoneNumber)
    }
    
    @IBAction func phoneNumberTextFieldDidChange(_ sender: Any) {
        
    }
    
}

extension SignInViewController: SignInView { }

private extension SignInViewController {
    
    func setupUI() {
//        phoneTextField.delegate = self
//        phoneTextField.placeholder = "+7ххххххххххх"
        
        titleLabel.text = "Введите номер"
        desciriptionLabel.text = "Мы вышлем на него код подтверждения"
        
        titleLabel.font = UIToken.font.get(.brandH1).font
        desciriptionLabel.font = UIToken.font.get(.mediumRegular).font
        
        actionButton.setTitle("Продолжить", for: .normal)
        
        phoneTextField.backgroundColor = UIColor(hex: "#F6F6F6")
        phoneTextField.keyboardType = .phonePad
    }
    
    func format(with mask: String, phone: String) -> String {
        let numbers = phone.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
        var result = ""
        var index = numbers.startIndex // numbers iterator

        // iterate over the mask characters until the iterator of numbers ends
        for ch in mask where index < numbers.endIndex {
            if ch == "X" {
                // mask requires a number in this place, so take the next one
                result.append(numbers[index])

                // move numbers iterator to the next index
                index = numbers.index(after: index)

            } else {
                result.append(ch) // just append a mask character
            }
        }
        return result
    }
}

extension SignInViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false }
        let newString = (text as NSString).replacingCharacters(in: range, with: string)
        textField.text = format(with: "+X (XXX) XXX-XXXX", phone: newString)
        return false
    }
}

extension SignInViewController: InputComponentDelegate {
    
    func textChanged(_ value: String) {
//        presenter.onEmailInputTextChanged(with: value)
//        let newString = (value as NSString).replacingCharacters(in: range, with: string)
//        textField.text = format(with: "+X (XXX) XXX-XXXX", phone: newString)
    }
}

extension String {
    func applyPatternOnNumbers(pattern: String, replacementCharacter: Character) -> String {
        var pureNumber = self.replacingOccurrences( of: "[^0-9]", with: "", options: .regularExpression)
        for index in 0 ..< pattern.count {
            guard index < pureNumber.count else { return pureNumber }
            let stringIndex = String.Index(utf16Offset: index, in: pattern)
            let patternCharacter = pattern[stringIndex]
            guard patternCharacter != replacementCharacter else { continue }
            pureNumber.insert(patternCharacter, at: stringIndex)
        }
        return pureNumber
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tapGesture = UITapGestureRecognizer(target: self,
                         action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
    }

    @objc func hideKeyboard() {
        view.endEditing(true)
    }
}
