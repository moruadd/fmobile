//
//  SignInRequestFactory.swift
//  F-Mobile
//
//  Created by Damir on 30.10.2023.
//

import Network
import Alamofire
import Foundation

enum SignInServiceRequestFactory: NetworkRequestConvertible {
    case getOtpCode(requestModel: SignInRequestModel)
    
    // MARK: - Computed Properties
    
    var method: APIMethod {
        switch self {
        case .getOtpCode:
            return .post
        }
    }
    
    var path: String {
        switch self {
        case .getOtpCode:
            return "/mvno/v1/auth/oauth/otp"
        }
    }
    
    var parameters: APIParameters? {
        switch self {
        case .getOtpCode(let requestModel):
            return requestModel.buildDictionary(using: .default)
        }
    }
}

