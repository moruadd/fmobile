//
//  SignInRequestModel.swift
//  F-Mobile
//
//  Created by Damir on 30.10.2023.
//

import Foundation

struct SignInRequestModel: Codable {
    let msisdn: String
}
