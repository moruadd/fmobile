//
//  SignInResponseModel.swift
//  F-Mobile
//
//  Created by Damir on 30.10.2023.
//

import Foundation

struct SignInResponseModel: Decodable {
    let code: String?
    let message: String?
    let data: SignInResponseData?
}

struct SignInResponseData: Decodable {
    let blockInterval: Int?
    let otpId: String?
}
