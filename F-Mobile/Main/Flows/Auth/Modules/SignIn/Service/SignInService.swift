//
//  SignInService.swift
//  F-Mobile
//
//  Created by Damir on 30.10.2023.
//

import Alamofire
import Network

protocol SignInService {
    func getOtpCode(with phoneNumber: String, callback: @escaping (Result<SignInResponseModel, NetworkError>) -> Void)
}

final class SignInServiceImpl {
        
    // MARK: - Dependency
    
    private let networkService: NetworkService
    
    // MARK: - Init
    
    init(networkService: NetworkService) {
        self.networkService = networkService
    }
    
    convenience init() {
        self.init(networkService: NetworkServiceImpl(type: .token))
    }
}

// MARK: - SignInService

extension SignInServiceImpl: SignInService {
    
    func getOtpCode(with phoneNumber: String, callback: @escaping (Result<SignInResponseModel, NetworkError>) -> Void) {
        networkService.performRequest(router: SignInServiceRequestFactory.getOtpCode(requestModel: SignInRequestModel(msisdn: phoneNumber))) { (result: Result<SignInResponseModel, NetworkError>) in
            switch result {
            case .success(let model):
                callback(.success(model))
            case .failure(let error):
                callback(.failure(error))
            }
        }
    }
}

