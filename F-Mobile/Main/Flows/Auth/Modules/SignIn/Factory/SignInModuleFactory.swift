//
//  SignInModuleFactory.swift
//  F-Mobile
//
//  Created by Damir on 30.10.2023.
//

import Fundamental

typealias SignInModuleFactory = AnyModuleFactory<AuthCoordinator, SignInPresenter>

struct SignInModuleFactoryImpl {}

// MARK: - ModuleFactory

extension SignInModuleFactoryImpl: ModuleFactory {
    
    func createModule(withCoordinator coordinator: AuthCoordinator) -> Module<SignInPresenter> {
        let viewController = SignInViewController.instantiate(with: .main)
        viewController.presenter = SignInPresenterImpl()
        viewController.presenter.coordinator = coordinator
        viewController.presenter.view = viewController
        return Module(view: viewController, presenter: viewController.presenter)
    }
}

