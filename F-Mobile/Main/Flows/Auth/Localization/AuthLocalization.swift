//
//  AuthLocalization.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import Foundation
import Fundamental

enum AuthLocalization: String, Localizable {
    case recoverySms
    case recoveryEmail
    case passwordNeedsChange
    case notSubscriber
    case corruptedStructure
    case disabledSIM
    case disabledSIMTitle
    case disabledSIMDescr
    case ok
}
