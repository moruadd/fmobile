//
//  PhoneNumberTextField.swift
//  F-Mobile
//
//  Created by Damir on 30.10.2023.
//

import UIKit
import Components

private enum Constants {
    static let codePrefix = "+7"
    static let attributedPlaceHolder = NSAttributedString(string: "+7 (___) ___ __ __",
                                                          attributes: [.foregroundColor: ColorFactory.color(for: .txSubtle)])
}

final class PhoneNumberTextField: BaseTextField {
    private let formatter: PhoneNumberFormatting = PropertyFormatter(appLanguage: .default)

    override func deleteBackward() {
        if let digits = formatter.rawPhoneNumber(from: text ?? "") {
            guard !digits.isEmpty else { return }
            let formattedText = formatter.formattedPhoneNumber(from: String(digits.dropLast())).value
            text = formattedText
        } else {
            text?.removeLast()
        }
        sendActions(for: .editingChanged)
    }

    override func setup() {
        super.setup()
        prefix = Constants.codePrefix
        attributedPlaceholder = Constants.attributedPlaceHolder
        backgroundColor = .white
        font = UIFont.systemFont(ofSize: 17)
        keyboardType = .numberPad
        textContentType = .telephoneNumber
        tintColor = ColorFactory.color(for: .brBlue)
    }

    override func editingDidBegin() {
        super.editingDidBegin()

        guard let text = text, text.isEmpty else { return }
        self.text = Constants.codePrefix
        sendActions(for: .editingChanged)
    }

    override func editingChanged() {
        guard isEditing else { return }
        guard let digits = formatter.rawPhoneNumber(from: text ?? "") else {
            deleteBackward()
            return
        }
        let (formattedText, offset) = formatter.formattedPhoneNumber(from: digits)
        text = formattedText
        if let newPosition = position(from: beginningOfDocument, offset: offset) {
            selectedTextRange = textRange(from: newPosition, to: newPosition)
        }
    }

    override func editingDidEnd() {
        super.editingDidEnd()
        guard formatter.rawPhoneNumber(from: text ?? "")?.isEmpty ?? true else { return }
        text = ""
        sendActions(for: .editingChanged)
    }
    
    func insertAndFormatText(_ text: String) {
        self.text = formatter.formattedPhoneNumber(from: text).value
    }
    
    func getRawText() -> String? {
        return formatter.rawPhoneNumber(from: text ?? "")
    }
    
    func showSkeletonableAnimating(usingColor: UIColor = ColorFactory.color(for: .bgSecondary)) {
        showSkeleton(usingColor: usingColor, transition: .none)
        text = " "
    }
    
    func hideSkeletonableAnimating() {
        hideSkeleton()
        guard let rawText = getRawText(), !rawText.isEmpty else { return }
        attributedPlaceholder = Constants.attributedPlaceHolder
    }
}

