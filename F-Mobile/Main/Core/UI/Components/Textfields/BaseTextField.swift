//
//  BaseTextField.swift
//  F-Mobile
//
//  Created by Damir on 30.10.2023.
//

import UIKit
import Components

private enum Constants {
    static let edgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
    static let placeholderEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
}

class BaseTextField: UITextField {
    enum State {
        case filled
        case unfilled
        case focused
        case disabled
    }

    var prefix: String = ""
    
    override var isEnabled: Bool {
        didSet {
            if !isEnabled {
                textFieldState = .disabled
            }
        }
    }

    private var textFieldState: State = .unfilled {
        didSet {
            updateAppearance(for: textFieldState)
        }
    }

    var annotationText: String? {
        didSet {
            setAnnotation(text: annotationText)
        }
    }

    private lazy var annotationLabel: UILabel = {
        let label = UILabel()
        label.font = FontFactory.font(for: .tel2ElRegular11)
        label.textColor = ColorFactory.color(for: .txSecondary)
        return label
    }()

    private lazy var bottomLineLayer: CALayer = {
        let layer = CALayer()
        layer.backgroundColor = ColorFactory.color(for: .elLine).cgColor
        return layer
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        setupActions()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
        setupActions()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        bottomLineLayer.frame = CGRect(x: 0, y: frame.height - 1, width: frame.width, height: 1)
    }

    func setup() {
//        layer.addSublayer(bottomLineLayer)
//        contentVerticalAlignment = .bottom
    }

    func setupActions() {
        addTarget(self, action: #selector(editingDidBegin), for: .editingDidBegin)
        addTarget(self, action: #selector(editingChanged), for: .editingChanged)
        addTarget(self, action: #selector(editingDidEnd), for: .editingDidEnd)
    }

    @objc
    func editingDidBegin() {
        textFieldState = .focused
    }

    @objc
    func editingChanged() {}

    @objc
    func editingDidEnd() {
        guard let text = text?.replacingOccurrences(of: ["-", "_", "(", ")"], with: "").trimmingCharacters(in: .whitespaces) else { return }

        if isEditing {
            textFieldState = .focused
        } else {
            textFieldState = (text.count == prefix.count) ? .unfilled : .filled
        }
    }

    private func updateAppearance(for newState: State) {
        switch newState {
        case .focused:
            bottomLineLayer.animateBackgroundColorChange(to: ColorFactory.color(for: .brBlue))
            textColor = ColorFactory.color(for: .txPrimary)
        case .filled:
            bottomLineLayer.animateBackgroundColorChange(to: UIColor.black)
            textColor = ColorFactory.color(for: .txPrimary)
        case .unfilled:
            bottomLineLayer.animateBackgroundColorChange(to: ColorFactory.color(for: .elLine))
            textColor = ColorFactory.color(for: .txPrimary)
        case .disabled:
            bottomLineLayer.animateBackgroundColorChange(to: ColorFactory.color(for: .elLine))
            textColor = ColorFactory.color(for: .txSubtle)
        }
    }

    private func setAnnotation(text: String?) {
        annotationLabel.text = text
        annotationLabel.sizeToFit()
        layoutIfNeeded()
        guard rightView == nil else { return }
        rightView = annotationLabel
        rightViewMode = .always
        rightView?.contentMode = .bottom
    }

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        bounds.inset(by: Constants.edgeInsets)
    }

    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        bounds.inset(by: Constants.placeholderEdgeInsets)
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        bounds.inset(by: Constants.edgeInsets)
    }
}

