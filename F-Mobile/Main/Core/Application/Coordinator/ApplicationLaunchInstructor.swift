//
//  ApplicationLaunchInstructor.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import Foundation

enum ApplicationLaunchInstruction {
    case auth
    case main
    case pinVerify
}

final class ApplicationLaunchInstructor {
    var flow: ApplicationLaunchInstruction {
        switch userSession.pinState {
        case .declined:
            return .main
        case .created:
            return .pinVerify
        case .notSet:
            return .auth
        }
    }

    private let userSession: UserSession

    init(userSession: UserSession) {
        self.userSession = userSession
    }
}

