//
//  ApplicationCoordinator.swift
//  F-Mobile
//
//  Created by Damir on 23.09.2023.
//

import Components
import UIKit
import Fundamental

protocol ApplicationCoordinatorProtocol {
    func show(message: String)
}

final class ApplicationCoordinator: BaseCoordinator {
    private let container: DependencyContainer
    private let launchInstructor: ApplicationLaunchInstructor
    private let coordinatorFactory: ApplicationCoordinatorFactory
    private let appSessionManager: AppSessionManager

    init(router: Router, container: DependencyContainer) {
        self.container = container
        launchInstructor = ApplicationLaunchInstructor(userSession: container.resolve(UserSession.self)!)
        coordinatorFactory = ApplicationCoordinatorFactory(container: container)
        appSessionManager = container.resolve(AppSessionManager.self)!
        super.init(router: router)
        setupCallbacks()
    }

    override func start() {
        switch launchInstructor.flow {
        case .auth:
            runAuthFlow()
        case .main:
            runMainFlow()
        case .pinVerify:
            runPinVerifyFlow()
        }
    }

    private func showAppStore(url: String) {
        let application = container.resolve(UIApplication.self)!
        guard let url = URL(string: url), application.canOpenURL(url) else { return }
        application.openUrlIfPossible(url)
    }

    private func runAuthFlow() {
        let coordinator = coordinatorFactory.makeAuthCoordinator(router: router)
        coordinator.onFlowDidFinish = { [weak self, weak coordinator] in
            self?.removeDependency(coordinator)
            self?.runMainFlow()
        }
        addDependency(coordinator)
        coordinator.start()
    }

    private func runMainFlow() {
//        let coordinator = coordinatorFactory.makeMainTabBarCoordinator(router: router)
//        addDependency(coordinator)
//        if let pushCategoryId = pushCategoryId {
//            coordinator.start(with: pushCategoryId)
//            self.pushCategoryId = nil
//        } else {
//            coordinator.start(with: deepLinkActionType)
//        }
    }

    private func runPinVerifyFlow() {
//        guard !baseChildCoordinators.contains(where: { $0 is PinVerifyCoordinator }) else { return}
//        let (coordinator, module) = coordinatorFactory.makePinVerify()
//        coordinator.onFlowDidFinish = { [weak self, weak coordinator] isVerified in
//            guard let self = self else { return }
//            self.removeDependency(coordinator)
//            if isVerified {
//                self.router.dismissModule()
//                self.runMainFlow()
//            } else {
//                let authStateObserver = self.container.resolve(AuthStateObserver.self)!
//                authStateObserver.tokenDidExpire()
//            }
//        }
//        addDependency(coordinator)
//        coordinator.start()
//        router.present(module, animated: false, modalPresentationStyle: .fullScreen)
    }

    private func setupCallbacks() {
        appSessionManager.onSessionExpired = { [weak self] in
            DispatchQueue.main.async {
                guard let self = self else { return }
                self.clearChildCoordinators()
                self.router.dismissModule()
                self.start()
            }
        }
    }
}

// MARK: - Private

private extension ApplicationCoordinator {

    func openNeededFlow() {
        switch launchInstructor.flow {
        case .auth:
            runAuthFlow()
        case .main:
            runMainFlow()
        case .pinVerify:
            runPinVerifyFlow()
        }
    }
}

extension ApplicationCoordinator: ApplicationCoordinatorProtocol {

    func show(message: String) {
        ToastView.show(with: message)
    }
}
