//
//  ApplicationModuleFactory.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

//final class ApplicationModuleFactory {
//    private let alertFactory: AlertFactory
//
//    init(container: DependencyContainer) {
//        alertFactory = container.resolve(AlertFactory.self)!
//    }
//
//    func makeDeepLinkUpdate() -> OneActionAlertPresentable {
//        let alert = alertFactory.makeOneActionAlert(title: HomeLocalization.deepLinkAlertTitle.localized,
//                                                    message: nil,
//                                                    actionTitle: HomeLocalization.deepLinkAlertButton.localized)
//        return alert
//    }
//
//    func makeAppForcedUpdate(model: ForcedAlertData) -> OneActionAlertPresentable {
//        let appForceUpdateAlert = alertFactory.makeOneActionAlert(title: model.title,
//                                                                  message: model.description,
//                                                                  actionTitle: model.primaryButton)
//        return appForceUpdateAlert
//    }
//
//    func makeAppSoftUpdate(model: SoftAlertData) -> TwoActionsAlertPresentable {
//        let appSoftUpdateAlert = alertFactory.makeTwoActionsAlert(title: model.title,
//                                                                  message: model.description,
//                                                                  leftActTitle: model.secondaryButton,
//                                                                  rightActTitle: model.primaryButton)
//        return appSoftUpdateAlert
//    }
//}

