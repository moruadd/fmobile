//
//  ApplicationCoordinatorFactory.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import Foundation
import Fundamental

final class ApplicationCoordinatorFactory {
    private let container: DependencyContainer
    
    init(container: DependencyContainer) {
        self.container = container
    }

    func makeAuthCoordinator(router: Router) -> Coordinator & AuthCoordinatorOutput {
        AuthCoordinator(router: router, container: container)
    }

//    func makeMainTabBarCoordinator(router: Router) -> Coordinator & TabBarCoordinator {
//        TabBarCoordinatorImpl(router: router, container: container)
//    }
    
//    func makePinVerify() -> (coordinator: Coordinator & PinVerifyCoordinatorOutput, module: Presentable?) {
//        let rootController = CoordinatorNavigationController(backBarButtonImage: ImageType.backUi.image)
//        let coordinator = PinVerifyCoordinator(router: RouterImpl(rootController: rootController), container: container)
//        return (coordinator, rootController)
//    }
}

