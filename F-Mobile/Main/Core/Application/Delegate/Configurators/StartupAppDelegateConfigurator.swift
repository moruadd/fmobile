//
//  StartupAppDelegateConfigurator.swift
//  F-Mobile
//
//  Created by Damir on 23.09.2023.
//

import UIKit
import Components
import Network
import Alamofire
import Fundamental

final class StartupAppDelegateConfigurator: AppDelegateConfigurator {
    
    private var applicationCoordinator: ApplicationCoordinator?
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        UIToken.configure(applicationType: .tele2)
        
        let rootController = CoordinatorNavigationController(backBarButtonImage: ImageType.backUi.image, closeBarButtonImage: ImageType.closeLine.image)
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        window?.rootViewController = rootController
        
        let appSessionManager = AppDelegate.assembler.resolver.resolve(AppSessionManager.self)!
        let userSession = AppDelegate.assembler.resolver.resolve(UserSession.self)!

        if appSessionManager.isFirstLaunch {
            userSession.finish()
            appSessionManager.isFirstLaunch = false
        }

        applicationCoordinator = ApplicationCoordinator(router: RouterImpl(rootController: rootController), container: AppDelegate.assembler.resolver)
//        AppDelegate.assembler.resolver.resolve(AuthStateObserver.self)?.setCoordinator(applicationCoordinator)
        applicationCoordinator?.start()
        return true
    }
}
