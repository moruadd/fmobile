//
//  UIAppDelegateConfigurator.swift
//  F-Mobile
//
//  Created by Damir on 23.09.2023.
//

import UIKit
import Components

private enum Constants {
    static let titleTextAttributes: [NSAttributedString.Key: Any] = [
        .font: FontFactory.font(for: .tele2CoSemibold14),
        .foregroundColor: ColorFactory.color(for: .txPrimary)
    ]
    static let offset = UIOffset(horizontal: -1000.0, vertical: 0.0)
    static let textViewAttributes: [NSAttributedString.Key: Any] = [
        .foregroundColor: ColorFactory.color(for: .brBlue)
    ]
}

final class UIAppDelegateConfigurator: AppDelegateConfigurator {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setupNavigationBar()
        setupTextView()
        setupTableView()
        return true
    }
    
    private func setupNavigationBar() {
        let offset = UIOffset(horizontal: -1000.0, vertical: 0.0)
        let navigationBarAppearance = UINavigationBar.appearance()
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithTransparentBackground()
            appearance.backgroundColor = .white
//            appearance.titleTextAttributes = Constants.titleTextAttributes
            appearance.shadowColor = .clear
            navigationBarAppearance.standardAppearance = appearance
            navigationBarAppearance.scrollEdgeAppearance = appearance
            navigationBarAppearance.compactAppearance = appearance
            
            let buttonAppearance = UIBarButtonItemAppearance(style: .plain)
            buttonAppearance.normal.titlePositionAdjustment = offset
            appearance.backButtonAppearance = buttonAppearance
        } else {
            UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(offset, for: .default)
            navigationBarAppearance.titleTextAttributes = Constants.titleTextAttributes
            navigationBarAppearance.barTintColor = .white
            navigationBarAppearance.barStyle = .black
            navigationBarAppearance.shadowImage = UIImage()
            navigationBarAppearance.isTranslucent = false
            navigationBarAppearance.setBackgroundImage(UIImage(), for: .default)
        }
        navigationBarAppearance.tintColor = ColorFactory.color(for: .txPrimary)
    }
    
    private func setupTextView() {
        let textView = UITextView.appearance()
        textView.linkTextAttributes = Constants.textViewAttributes
    }
    
    private func setupTableView() {
        if #available(iOS 15.0, *) {
            UITableView.appearance().sectionHeaderTopPadding = .leastNormalMagnitude
            UITableView.appearance().sectionHeaderHeight = 0
            UITableView.appearance().tableHeaderView = UIView()
        }
    }
}

