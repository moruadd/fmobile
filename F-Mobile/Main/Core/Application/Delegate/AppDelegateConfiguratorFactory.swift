//
//  AppDelegateConfiguratorFactory.swift
//  F-Mobile
//
//  Created by Damir on 23.09.2023.
//

import Swinject

typealias DependencyContainer = Resolver

enum AppDelegateConfiguratorFactory {
    static func makeDefault() -> AppDelegateConfigurator {
        
        let startUpConfigurator = StartupAppDelegateConfigurator()
            
        return CompositeAppDelegateConfigurator(configurators: [
            startUpConfigurator,
//            ThirdPartiesAppDelegateConfigurator(
//                notificationCenter: NotificationCenter.default,
//                userSession: AppDelegate.assembler.resolver.resolve(UserSession.self)!
//            ),
            UIAppDelegateConfigurator()
        ])
    }
}

