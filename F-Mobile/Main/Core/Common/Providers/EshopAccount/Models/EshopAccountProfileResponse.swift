//
//  EshopAccountProfileResponse.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import Foundation

typealias EshopAccountProfile = EshopAccountProfileResponse

struct EshopAccountProfileResponse: Decodable {
    let contactPhone: String
    let msisdn: String
}

