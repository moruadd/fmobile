//
//  RegularAccountProfileResponse.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import Foundation

typealias RegularAccountProfile = RegularAccountProfileResponse

struct RegularAccountProfileResponse: Decodable {
    enum CodingKeys: String, CodingKey {
        case country
        case tariff
        case phoneNumber = "phone"
        case isUserInRoaming = "userInRoamingNow"
        case authEmail
        case notificationEmail
        case accounts = "boundPhoneNumbers"
        case additionalPhoneNumber = "contactPhone"
        case subscriberId
    }

    let country: Country
    let tariff: Tariff
    let phoneNumber: String
    let isUserInRoaming: Bool
    var authEmail: String?
    var notificationEmail: String?
    let accounts: [RegularAccount]
    var additionalPhoneNumber: String?
    var subscriberId: UInt?
}

struct Country: Decodable {
    let name: String
    let code: String
}

struct Tariff: Decodable {
    let id: Int
    let title: String?
}

struct RegularAccount: Decodable, Equatable {
    let caption: String
    let msisdn: String
    let name: String
    let isMain: Bool

    init(name: String, caption: String, msisdn: String, isMain: Bool) {
        self.name = name
        self.caption = caption
        self.msisdn = msisdn
        self.isMain = isMain
    }

    static func == (lhs: Self, rhs: Self) -> Bool {
        lhs.msisdn == rhs.msisdn && lhs.caption == rhs.caption
    }
}

