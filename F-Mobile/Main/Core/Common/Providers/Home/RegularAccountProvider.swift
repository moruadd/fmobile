//
//  RegularAccountProvider.swift
//  F-Mobile
//
//  Created by Damir on 30.10.2023.
//

import AlabsNetwork
import Foundation
import Promises

protocol RegularAccountProvider {
    func getRegularAccountProfile() -> Promise<RegularAccountProfileResponse>
}

extension Provider: RegularAccountProvider {
    func getRegularAccountProfile() -> Promise<RegularAccountProfileResponse> {
        rest.request(
            builder.set(path: "/user/profile/me")
                   .build()
        )
    }
}

