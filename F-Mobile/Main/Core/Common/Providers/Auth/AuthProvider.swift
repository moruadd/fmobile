//
//  AuthProvider.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import AlabsNetwork
import Alamofire
import Promises

enum AuthUserType: Equatable {
    case regular
}

protocol AuthProvider {
    func getAppVersionStatus(appVersion: String) -> Promise<AppVersionUpdatingUrgency>
    func getClientStatus(msisdn: String) -> Promise<AuthClientStatus>
    func getClientStatus(email: String) -> Promise<AuthClientStatus>
    func postToken(msisdn: String, password: String) -> Promise<TokenResponse>
    func postToken(email: String, password: String) -> Promise<TokenResponse>
    func postToken(_ refreshToken: String) -> Promise<TokenResponse>
    func postEshopToken(email: String, password: String) -> Promise<TokenResponse>
    func getCheckOperator(msisdn: String, callback: @escaping (Result<CheckClientResponse, Error>) -> Void)
}

extension Provider: AuthProvider {
    func getAppVersionStatus(appVersion: String) -> Promise<AppVersionUpdatingUrgency> {
        rest.request(
            builder.set(path: "/aux/check-version")
                   .set(method: .get)
                   .set(parameters: [
                       "platform": "ios",
                       "version": appVersion
                   ])
                   .build()
        )
    }

    func getClientStatus(msisdn: String) -> Promise<AuthClientStatus> {
        rest.request(
            builder.set(path: "/auth/client-status")
                   .set(method: .get)
                   .set(parameters: [
                       "msisdn": msisdn
                   ])
                   .build()
        )
    }

    func getClientStatus(email: String) -> Promise<AuthClientStatus> {
        rest.request(
            builder.set(path: "/auth/client-status")
                   .set(method: .get)
                   .set(parameters: [
                       "email": email
                   ])
                   .build()
        )
    }

    func postToken(msisdn: String, password: String) -> Promise<TokenResponse> {
        rest.request(
            builder.set(path: "/auth/oauth/token")
                   .set(method: .post)
                   .set(headers: .init(["Authorization": basicAuth]))
                   .set(parameters: [
                       "username": msisdn,
                       "password": password,
                       "grant_type": "msisdn_password"
                   ])
                   .build()
        )
    }

    func postToken(email: String, password: String) -> Promise<TokenResponse> {
        rest.request(
            builder.set(path: "/auth/oauth/token")
                   .set(method: .post)
                   .set(headers: .init(["Authorization": basicAuth]))
                   .set(parameters: [
                       "username": email,
                       "password": password,
                       "grant_type": "email_password"
                   ])
                   .build()
        )
    }

    func postEshopToken(email: String, password: String) -> Promise<TokenResponse> {
        rest.request(
            builder.set(path: "/auth/oauth/token")
                   .set(method: .post)
                   .set(headers: .init(["Authorization": basicAuth]))
                   .set(parameters: [
                       "username": email,
                       "password": password,
                       "grant_type": "eshop_password"
                   ])
                   .build()
        )
    }

    func postToken(_ refreshToken: String) -> Promise<TokenResponse> {
        rest.request(
            builder.set(path: "/auth/oauth/token")
                   .set(method: .post)
                   .set(headers: .init(["Authorization": basicAuth]))
                   .set(parameters: [
                       "refresh_token": refreshToken,
                       "grant_type": "refresh_token"
                   ])
                   .build()
        )
    }

    func getCheckOperator(msisdn: String, callback: @escaping (Result<CheckClientResponse, Error>) -> Void) {
        let parameters: Parameters = [
            "msisdn": msisdn
        ]
        rest.request(
            builder.set(path: "/auth/check-operator/")
                   .set(method: .get)
                   .set(parameters: parameters)
                   .build()
        )
        .then { (result: CheckClientResponse) in
            callback(.success(result))
        }
        .catch { error in
            callback(.failure(error))
        }
    }
}

extension Provider: NetworkAuthProviding {
    func postToken(_ refreshToken: String) -> DataRequest {
        let parameters: Parameters = [
            "operatorType": "TELE2",
            "platform": "ios",
            "clientType": "MOBILE",
            "refresh_token": refreshToken,
            "grant_type": "refresh_token"
        ]
        return rest.post("/auth/oauth/token", parameters: parameters, headers: ["Authorization": basicAuth])
    }
}

