//
//  CheckClientResponse.swift
//  F-Mobile
//
//  Created by Damir on 30.10.2023.
//

import Foundation

struct CheckClientResponse: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case isClient = "matchOperator"
        case message
    }
    
    let isClient: Bool
    let message: String?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        isClient = try container.decode(Bool.self, forKey: .isClient)
        message = try container.decodeIfPresent(String.self, forKey: .message)
    }
}

