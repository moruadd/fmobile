//
//  AppVersionUpdatingUrgency.swift
//  F-Mobile
//
//  Created by Damir on 30.10.2023.
//

import AlabsNetwork
import Alamofire
import Promises

enum AppVersionUpdatingUrgencyType: String, Decodable {
    case force
    case soft
    case ok
}

struct ForcedAlertData: Decodable {
    let title: String?
    let description: String?
    let primaryButton: String?
    let link: String?
}

struct SoftAlertData: Decodable {
    let title: String?
    let description: String?
    let primaryButton: String?
    let secondaryButton: String?
    let link: String?
}

enum AppVersionUpdatingUrgencyData {
    case force(model: ForcedAlertData)
    case soft(model: SoftAlertData)
    case ok
}

struct AppVersionUpdatingUrgency: Decodable {
    private enum CodingKeys: String, CodingKey {
        case type
        case data
    }
    
    let data: AppVersionUpdatingUrgencyData
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let urgencyType = try container.decodeIfPresent(AppVersionUpdatingUrgencyType.self, forKey: .type)
        switch urgencyType {
        case .force:
            let model = try container.decode(ForcedAlertData.self, forKey: .data)
            data = .force(model: model)
        case .soft:
            let model = try container.decode(SoftAlertData.self, forKey: .data)
            data = .soft(model: model)
        case .none, .ok:
            data = .ok
        }
    }
}

