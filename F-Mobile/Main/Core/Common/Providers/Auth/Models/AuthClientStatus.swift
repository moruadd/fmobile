//
//  AuthClientStatus.swift
//  F-Mobile
//
//  Created by Damir on 30.10.2023.
//

import Foundation

struct AuthClientStatus: Decodable {
    enum CodingKeys: String, CodingKey {
        case hasEmail
        case loginTransition
        case message
        case isMbb = "mbb"
        case alert
        case form
    }

    enum LoginTransitionType: String, Decodable {
        case showMessage
        case gotoLoginPage
        case gotoRegisterPage
        case goToEshop
        case goToSetPassword
    }
    
    struct Alert: Decodable {
        let title: String?
        let button: Button?
        let description: String?
    }
    
    struct Form: Decodable {
        let title: String?
        let button: Button?
        let description: String?
    }
    
    struct Button: Decodable {
        let title: String?
        let action: String?
    }

    let hasEmail: Bool
    let loginTransition: LoginTransitionType
    let message: String?
    let isMbb: Bool?
    let alert: Alert?
    let form: Form?
}

enum AuthTransitionType {
    case showMessage(message: String?)
    case gotoLoginPage(hasEmail: Bool)
    case goToRegisterPage(isMbb: Bool)
    case goToSetPassword

    init(status: AuthClientStatus) {
        switch status.loginTransition {
        case .showMessage:
            self = .showMessage(message: status.message)
        case .gotoLoginPage:
            self = .gotoLoginPage(hasEmail: status.hasEmail)
        case .gotoRegisterPage:
            self = .goToRegisterPage(isMbb: status.isMbb ?? false)
        case .goToEshop:
            self = .gotoLoginPage(hasEmail: status.hasEmail)
        case .goToSetPassword:
            self = .goToSetPassword
        }
    }
}

