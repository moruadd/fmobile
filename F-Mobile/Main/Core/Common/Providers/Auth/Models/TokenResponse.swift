//
//  TokenResponse.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import Foundation

struct TokenResponse: Decodable {
    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case refreshToken = "refresh_token"
        case subscriberId
    }

    let accessToken: String
    let refreshToken: String
    let subscriberId: UInt
}

