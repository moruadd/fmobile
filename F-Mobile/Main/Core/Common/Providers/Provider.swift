//
//  Provider.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import AlabsNetwork

class Provider {
    let rest: AlamofireNetwork<ProviderError>
    let basicAuth: String
    let builder: NetworkRequestBuilder

    init(rest: AlamofireNetwork<ProviderError>, basicAuth: String, builder: NetworkRequestBuilder) {
        self.rest = rest
        self.basicAuth = basicAuth
        self.builder = builder
    }
}

class ProviderError: AlamofireNetworkError {}

