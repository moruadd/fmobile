//
//  ImageType.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import Foundation
import Fundamental

enum ImageType: String, ImageFactory {
    var bundle: Bundle {
        .main
    }
    
    case accountCircle = "account_circle"
    case add = "add"
    case antispam = "antispam"
    case autopaymentMenuIcon = "autopayment-menu-icon"
    
    case backUi = "back-ui"
    case bestseller = "bestseller"
    case bottomRightTip = "bottom-right-tip"
    case bxlTelegram = "bxl_telegram"
    
    case calendarMonth = "calendar-month"
    case callBack = "call-back"
    case callCancelled = "call-cancelled"
    case callForward = "call-forward"
    case callTalking = "call-talking"
    case call = "call"
    case checkFill = "check-fill"
    case checkboxCircleLineSuccess = "checkbox-circle-line-success"
    case checkmarkCircle = "checkmark_circle"
    case chevronDown = "chevron-down"
    case chevronRight = "chevron-right"
    case chevronLeft = "chevron-left"
    case clear = "clear"
    case closeLine = "close-line"
    case closeButton = "close-button"
    case contactNotebook = "contact-notebook"
    case coupon = "coupon"
    case creditCardOutlined = "credit_card_outlined"
    
    case dashboardVsOutlined = "dashboard-vs-outlined"
    case dashboardVs = "dashboard-vs"
    case deleteBackLine = "delete-back-line"
    case deleteOutlined = "delete-outlined"
    case detalization = "detalization"
    case documentTextOutlined = "document-text-outlined"
    case double = "double"
    case downloadTo = "download-to"
    
    case earthEast = "earth-east"
    case earthIn = "earth-in"
    case earthOut = "earth-out"
    case earthWest = "earth-west"
    case emailOutlined = "email-outlined"
    case errorOutlined = "error-outlined"
    case errorWarningLine = "error-warning-line"
    case eshopHome = "eshop-home"
    case eyeCloseLine = "eye-close-line"
    case eyeLine = "eye-line"
    
    case faceIdLine = "face-id-line"
    case facebook = "facebook"
    case fileOutlined = "file-outlined"
    case finance = "finance"
    
    case gamepadLine = "gamepad-line"
    case gift = "gift"
    case giftCardFill = "giftcard-fill"
    case globe = "globe"
    case groupAlert = "group-alert"
    case groupDelete = "group-delete"
    case groupEdit = "group-edit"
    case groupEqual = "group-equal"
    case groupPlus = "group-plus"
    case groupSenior = "group-senior"
    case groupUserSelected = "group-user-selected"
    case groupUserUnselected = "group-user-unselected"
    
    case history = "history"
    case home = "home"
    
    case illustrationAirplane = "illustration-airplane"
    case illustrationDodo = "illustration-dodo"
    case illustrationEarth = "illustration-earth"
    case illustrationPhone = "illustration-phone"
    case infinity = "infinity"
    case infoOutined = "info_outined"
    case instagram = "instagram"
    case iphone = "iphone"
    
    case joystick = "joystick"
    
    case lightbulb = "lightbulb"
    case logoBlack = "logo-black"
    case logoutBoxRLine = "logout-box-r-line"
    
    case mobFinance = "mob-finance"
    case moreHorizontal = "more-horizontal"
    
    case newMessage = "new-message"
    case newReleaseOutlined = "new_release_outlined"
    case noComputerLine = "no-computer-line"
    case notificationOn = "notification-on"
    
    case odnoklassniki = "odnoklassniki"
    
    case pTestDark = "p-test-dark"
    case pTestLight = "p-test-light"
    case planeLine = "plane-line"
    case play = "play"
    case poi = "poi"
    case pushNotificationOff = "push-notification-off"
    case pushNotificationOn = "push-notification-on"
    case pushSoundOff = "push-sound-off"
    case pushTrash = "push-trash"
    
    case qrBus = "qr-bus"
    case qrTenge = "qr-tenge"
    
    case refresh = "refresh"
    case registrationSuccess = "registration-success"
    case remainingTime = "remaining-time"
    
    case sad = "sad"
    case savings = "savings"
    case scan = "scan"
    case search = "search"
    case settingsOutlined = "settings-outlined"
    case shoppingCartLine = "shopping-cart-line"
    case simCardChange = "sim-card-change"
    case simCardLine = "sim-card-line"
    case simCard = "sim-card"
    case smsChat = "sms-chat"
    case smsOutlined = "sms-outlined"
    case support = "support"
    case swapHorizontal = "swap-horizontal"
    
    case teleAitu = "tele-aitu"
    case tele2 = "tele2"
    case teleDevicesIphone = "teleDevicesIphone"
    case teleFinanceShoppingBagLine = "teleFinanceShoppingBagLine"
    case userOutlined = "user_outlined"
    case telegram = "telegram"
    case thumb = "thumb"
    case tiktok = "tiktok"
    case topSecurityOutlined = "top_security_outlined"
    case touchIdLine = "touch-id-line"
    case transform = "transform"
    case triangle = "triangle"
    case twitch = "twitch"
    
    case union = "union"
    case update = "update"
    case viber = "viber"
    case virus = "virus"
    case vk = "vk"
    
    case walletOutlined = "wallet-outlined"
    case weather = "weather"
    case whatsappOutlined = "whatsapp_outlined"
    case whatsapp = "whatsapp"
    case wheelShadow = "wheel-shadow"
    case wifiOff = "wifi-off"
    
    case youtube = "youtube"
    
    case medeuBg = "medeu-bg"
    case ticket
    case emptyBox = "empty-box"
    
    case blueAward = "icon_blue_award"
    case goldAward = "icon_gold_award"
    case defaultAward = "icon_default_award"
    
    case radioButtonEnabled = "radio-button-enabled"
    case radioButtonDisabled = "radio-button-disabled"
    
    case emptyPage = "empty-page"
    
    case instashopBanner = "instashopBanner"
}
