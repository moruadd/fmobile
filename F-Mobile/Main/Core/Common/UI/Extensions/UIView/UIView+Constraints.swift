//
//  UIView+Constraints.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import UIKit

extension UIView {
    func setLayoutSizeConstraints() {
        translatesAutoresizingMaskIntoConstraints = false
        let layoutSize = systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        heightAnchor.constraint(equalToConstant: layoutSize.height).isActive = true
        widthAnchor.constraint(equalToConstant: layoutSize.width).isActive = true
    }
    
    func setHeightConstraint(_ height: CGFloat) {
        translatesAutoresizingMaskIntoConstraints = false
        heightAnchor.constraint(equalToConstant: height).isActive = true
    }
    
    func setWidthConstraint(_ width: CGFloat) {
        translatesAutoresizingMaskIntoConstraints = false
        widthAnchor.constraint(equalToConstant: width).isActive = true
    }
    
    func setSizeConstraints(width: CGFloat, height: CGFloat) {
        translatesAutoresizingMaskIntoConstraints = false
        widthAnchor.constraint(equalToConstant: width).isActive = true
        heightAnchor.constraint(equalToConstant: height).isActive = true
    }
    
    func setCenterConstaints(by view: UIView) {
        translatesAutoresizingMaskIntoConstraints = false
        centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    func addSubviewMatchParent(_ subview: UIView,
                               topMargin: CGFloat = 0.0,
                               bottomMargin: CGFloat = 0.0,
                               leftMargin: CGFloat = 0.0,
                               rightMargin: CGFloat = 0.0) {
        addSubview(subview)
        subview.translatesAutoresizingMaskIntoConstraints = false
        subview.topAnchor.constraint(equalTo: topAnchor, constant: topMargin).isActive = true
        subview.leadingAnchor.constraint(equalTo: leadingAnchor, constant: leftMargin).isActive = true
        subview.trailingAnchor.constraint(equalTo: trailingAnchor, constant: rightMargin).isActive = true
        subview.bottomAnchor.constraint(equalTo: bottomAnchor, constant: bottomMargin).isActive = true
    }
    
    @discardableResult
    func addConstrainedSubview(_ subview: UIView, constrain: NSLayoutConstraint.Attribute...) -> [NSLayoutConstraint] {
        return addConstrainedSubview(subview, constrainedAttributes: constrain)
    }
    
    @discardableResult
    func addConstrainedSubview(_ subview: UIView, constrainedAttributes: [NSLayoutConstraint.Attribute]) -> [NSLayoutConstraint] {
        subview.translatesAutoresizingMaskIntoConstraints = false
        addSubview(subview)
        return constrainedAttributes.map { self.constrain(subview, at: $0) }
    }
    
    func addConstraintWithoutConflict(_ constraint: NSLayoutConstraint) {
        removeConstraints(constraints.filter {
            constraint.firstItem === $0.firstItem
                && constraint.secondItem === $0.secondItem
                && constraint.firstAttribute == $0.firstAttribute
                && constraint.secondAttribute == $0.secondAttribute
        })
        addConstraint(constraint)
    }
    
    @discardableResult
    func constrain(
        _ subview: UIView,
        at: NSLayoutConstraint.Attribute,
        to subview2: UIView,
        at at2: NSLayoutConstraint.Attribute = .notAnAttribute,
        diff: CGFloat = 0,
        ratio: CGFloat = 1,
        relation: NSLayoutConstraint.Relation = .equal,
        priority: UILayoutPriority = .required,
        identifier: String? = nil
    ) -> NSLayoutConstraint {
        let at2real = at2 == .notAnAttribute ? at : at2
        let constraint = NSLayoutConstraint(
            item: subview,
            attribute: at,
            relatedBy: relation,
            toItem: subview2,
            attribute: at2real,
            multiplier: ratio,
            constant: diff
        )
        constraint.priority = priority
        constraint.identifier = identifier
        addConstraintWithoutConflict(constraint)
        return constraint
    }
    @discardableResult
    func constrain(
        _ subview: UIView,
        at: NSLayoutConstraint.Attribute,
        diff: CGFloat = 0,
        ratio: CGFloat = 1,
        relation: NSLayoutConstraint.Relation = .equal,
        priority: UILayoutPriority = .required,
        identifier: String? = nil
    ) -> NSLayoutConstraint {
        let constraint = NSLayoutConstraint(
            item: subview,
            attribute: at,
            relatedBy: relation,
            toItem: self,
            attribute: at,
            multiplier: ratio,
            constant: diff
        )
        constraint.priority = priority
        constraint.identifier = identifier
        addConstraintWithoutConflict(constraint)
        return constraint
    }
}

