//
//  getStatusBarHeight.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import UIKit

/// Возвращает высоту до верхней границы navigationBar-a (statusBarHeight + safeAreaInsets).
/// Функции объявлена `internal` в явном виде, чтобы подчеркнуть что её лучше использовать в конкретном контексте `DPUIKit`,
/// нежели чем в основном приложении.
internal func getStatusBarHeight() -> CGFloat {
    var statusBarHeight: CGFloat = 0
    if #available(iOS 13.0, *) {
        let window = UIApplication.shared.windows.first { $0.isKeyWindow }
        statusBarHeight = window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
    } else {
        statusBarHeight = UIApplication.shared.statusBarFrame.height
    }
    return statusBarHeight
}

