//
//  CALayer+Color.swift
//  F-Mobile
//
//  Created by Damir on 30.10.2023.
//

import UIKit

extension CALayer {
    func animateBackgroundColorChange(to color: UIColor, duration: Double = 0.2) {
        let animation = CABasicAnimation(keyPath: "backgroundColor")
        animation.fromValue = backgroundColor
        animation.toValue = color.cgColor
        animation.duration = duration
        add(animation, forKey: "backgroundColor")
        backgroundColor = color.cgColor
    }
}

