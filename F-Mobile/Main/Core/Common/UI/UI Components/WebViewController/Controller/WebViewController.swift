//
//  WebViewController.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import UIKit
import WebKit

protocol WebPresentable: Presentable {
    var onCloseButtonDidtap: Callback? { get set }
    var onBackButtonDidTap: Callback? { get set }
    var onLoginButtonDidTap: Callback? { get set }
    var onErrorDidOccur: ((StatefulTableView, Error) -> Void)? { get set }
    var onUrlDidTap: ((_ url: URL) -> Void)? { get set }
    
    func reloadWebView()
}

protocol WebCookiesStorable {
    var webBaseUrl: String { get }
    var cookieCollection: CookieCollection { get }
}

private enum Constants {
    static let defaultRowCount = 15
    static let animationDuration: TimeInterval = 0.3
    static let cookieHTTPHeaderField = "Cookie"
    static let webAppInterface: String = "iosCallbackHandler"
}

class WebViewController: ViewController, WebPresentable, WKUIDelegate {
    var onCloseButtonDidtap: Callback?
    var onBackButtonDidTap: Callback?
    var onLoginButtonDidTap: Callback?
    var onErrorDidOccur: ((StatefulTableView, Error) -> Void)?
    var onUrlDidTap: ((_ url: URL) -> Void)?
    
    // swiftlint:disable unowned_variable_capture
    private lazy var webView: WKWebView = { [unowned self] in
        let contentController = WKUserContentController()
        contentController.add(WeakWKScriptMessageHandler(delegate: self), name: Constants.webAppInterface)

        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true

        let configuration = WKWebViewConfiguration()
        configuration.userContentController = contentController
        configuration.preferences = preferences
        
        let script = cookieCollection.getJsCookiesString(for: cookiesStorage.webBaseUrl)
        let cookieScript = WKUserScript(source: script, injectionTime: .atDocumentStart, forMainFrameOnly: false)
        configuration.userContentController.addUserScript(cookieScript)

        let webView = WKWebView(frame: .zero, configuration: configuration)
        return webView
    }()
    
    private var rowCount = Constants.defaultRowCount
    private let store: WebStore
    private let appearance: WebViewControllerAppearance
    private let cookieCollection: CookieCollection
    private let cookiesStorage: WebCookiesStorable
    
    @IBOutlet private var tableView: StatefulTableView!
    @IBOutlet private var webViewContainer: UIView!

    init(store: WebStore, appearance: WebViewControllerAppearance, cookiesStorage: WebCookiesStorable) {
        self.store = store
        self.appearance = appearance
        self.cookieCollection = cookiesStorage.cookieCollection
        self.cookiesStorage = cookiesStorage
        super.init(nibName: String(describing: Self.self), bundle: Bundle(for: Self.self))
    }

    required init?(coder: NSCoder) {
        nil
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupObservers()
        reloadWebView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
    }

    override func viewWillDisappear(_ animated: Bool) {
        cookieCollection.clearCookies()
    }
    
    override func customBackButtonDidTap() {
        onBackButtonDidTap?()
    }

    override func customCloseButtonDidTap() {
        onCloseButtonDidtap?()
    }
    
    func reloadWebView() {
        rowCount = Constants.defaultRowCount
        store.dispatch(action: .didPrepareForLoading)
    }
    
    private func setupNavigationBar() {
        if #available(iOS 13.0, *), let appearence = navigationController?.navigationBar.standardAppearance {
            appearence.backgroundColor = appearance.navigationBarTintColor
            appearence.titleTextAttributes = appearance.navigationTitleTextAttributes
            navigationController?.navigationBar.standardAppearance = appearence
            navigationController?.navigationBar.scrollEdgeAppearance = appearence
            navigationController?.navigationBar.compactAppearance = appearence
        } else {
            navigationController?.navigationBar.titleTextAttributes = appearance.navigationTitleTextAttributes
            navigationController?.navigationBar.barTintColor = appearance.navigationBarTintColor
            navigationController?.navigationBar.barStyle = .default
        }
        
        navigationItem.title = appearance.navigationTitle
        navigationController?.navigationBar.barTintColor = appearance.navigationBarTintColor
        navigationItem.leftBarButtonItem = nil
        navigationItem.hidesBackButton = false
    }

    private func setupUI() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(cellClass: WebViewSkeletonableCell.self, bundle: .main)
        tableView.skeletonTintColor = appearance.skeletonTintColor
        setupWebView()
    }

    private func setupWebView() {
        webView.contentMode = .scaleToFill
        webView.uiDelegate = self
        webView.navigationDelegate = self
        webViewContainer.addSubviewMatchParent(webView)
    }

    private func setupObservers() {
        store.$state.observe(self) { vc, state in
            guard let state = state else { return }
            switch state {
            case .loading:
                vc.tableView.setState(.loading)
            case .loaded:
                vc.tableView.setState(.loaded(animated: true))
                vc.showWebView()
            case let .urlPrepared(url):
                vc.loadWebView(url: url)
            case .closeTapped:
                vc.onCloseButtonDidtap?()
                vc.onBackButtonDidTap?()
            case .showLogin:
                vc.onLoginButtonDidTap?()
            case let .urlTapped(url):
                vc.onUrlDidTap?(url)
            }
        }
    }
    
    private func loadWebView(url: URL) {
        rowCount = Constants.defaultRowCount
        var request = URLRequest(url: url)
        request.addValue(cookieCollection.makeCookieHeader(), forHTTPHeaderField: Constants.cookieHTTPHeaderField)
        webView.load(request)
    }
    
    private func showWebView() {
        UIView.animate(withDuration: Constants.animationDuration,
                       animations: {
                           self.tableView.alpha = 0
                           self.webViewContainer.alpha = 1
        }, completion: nil)
    }
    
    deinit {
        self.webView.configuration.userContentController.removeScriptMessageHandler(forName: Constants.webAppInterface)
        self.webView.configuration.userContentController.removeAllUserScripts()
    }
}

extension WebViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        rowCount
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: WebViewSkeletonableCell = tableView.dequeueReusableCell(for: indexPath)
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        indexPath.row == 0 ? 120 : 72
    }
}

extension WebViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        store.dispatch(action: .didFinishLoading)
    }

    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        rowCount = 0
        onErrorDidOccur?(tableView, WebError.common)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let requestUrl = navigationAction.request.url, requestUrl.scheme == "tel" {
            UIApplication.shared.open(requestUrl, options: [:], completionHandler: nil)
            decisionHandler(.cancel)
            
        } else {
            decisionHandler(.allow)
        }
    }
}

extension WebViewController: WKScriptMessageHandler {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if message.name == Constants.webAppInterface {
            store.dispatch(action: .didReceiveScriptMessage(message: message))
        }
    }
}

class WeakWKScriptMessageHandler: NSObject, WKScriptMessageHandler {
    weak var delegate: WKScriptMessageHandler?
    init(delegate: WKScriptMessageHandler) {
        self.delegate = delegate
        super.init()
    }
    func userContentController(_ userContentController: WKUserContentController,
                               didReceive message: WKScriptMessage) {
        self.delegate?.userContentController(
            userContentController, didReceive: message)
    }
}

