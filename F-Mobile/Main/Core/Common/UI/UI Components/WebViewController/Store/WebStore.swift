//
//  WebStore.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import Foundation
import WebKit

final class WebStore {
    enum Action {
        case didPrepareForLoading
        case didFinishLoading
        case didReceiveScriptMessage(message: WKScriptMessage)
    }

    enum State {
        case loading
        case loaded
        case closeTapped
        case showLogin
        case urlTapped(url: URL)
        case urlPrepared(url: URL)
    }

    enum ErrorType {
        case content(error: Error)
        case request(message: String)
    }
    
    private let decoder = ScriptMessageDecoder()
    private let urlResolver: UrlResolver
    private let url: String
    
    @Observable private(set) var state: State?
    
    init(url: String, urlResolver: UrlResolver) {
        self.url = url
        self.urlResolver = urlResolver
    }

    func dispatch(action: Action) {
        switch action {
        case .didPrepareForLoading:
            state = .loading
            emitUrlPreparedState()
        case .didFinishLoading:
            state = .loaded
        case let .didReceiveScriptMessage(message):
            guard let action = decoder.decodeScriptMessage(message: message) else { return }
            handleWebAction(action: action)
        }
    }
    
    private func emitUrlPreparedState() {
        guard let url = urlResolver.resolve(url: url) else { return }
        state = .urlPrepared(url: url)
    }
    
    private func handleWebAction(action: ScriptMessageAction) {
        switch action.type {
        case .close:
            state = .closeTapped
        case .login:
            state = .showLogin
        case .openUrl:
            guard let data = action.data as? ScriptMessageUrlActionData,
                  let urlString = data.url,
                  let url = URL(string: urlString)
            else { return }
            state = .urlTapped(url: url)
        case .unknown:
            break
        }
    }

}

