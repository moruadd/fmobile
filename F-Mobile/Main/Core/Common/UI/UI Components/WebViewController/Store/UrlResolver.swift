//
//  UrlResolver.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import UIKit

private enum Constants {
    static let regexPattern = #"(applewebdata://)((\w|-)+)(([.]|[/]))"#
}

protocol UrlResolver {
    func resolve(url: String) -> URL?
}

class RegexedUrlResolver: UrlResolver {
    private let baseUrl: URL?
    
    init(baseUrl: String) {
        self.baseUrl = URL(string: baseUrl)
    }
    
    func resolve(url: String) -> URL? {
        let relativePath = url.replacingOccurrences(
            of: Constants.regexPattern,
            with: "/",
            options: .regularExpression
        )
        let resolvedUrl = URL(string: relativePath, relativeTo: baseUrl)
        return resolvedUrl?.absoluteURL
    }
}

