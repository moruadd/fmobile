//
//  Cookie.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import Foundation
import WebKit

enum Cookie: String {
    case language = "i18next_lang"
    case clientType = "clientType"
    case accessToken = "accessToken"
    case refreshToken = "refreshToken"
    case activeMsisdn = "activeMsisdn"
    case mode = "mode"
    case version = "version"
}

typealias CookieCollection = [Cookie: String]

extension Dictionary where Key == Cookie, Value == String {
    func makeCookieHeader() -> String {
        self.map { "\($0.key.rawValue)=\($0.value);" }.joined()
    }
    
    func getJsCookiesString(for webBaseUrl: String) -> String {
        var jsString = ""
        for cookie in makeCookies(for: webBaseUrl) {
            jsString += "document.cookie='\(cookie.name)=\(cookie.value); domain=\(cookie.domain); path=\(cookie.path); "
            jsString += "'; "
        }
        return jsString
    }
    
    func clearCookies() {
        let dataStore = WKWebsiteDataStore.default()
        dataStore.fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { records in
            dataStore.removeData(
                ofTypes: WKWebsiteDataStore.allWebsiteDataTypes(),
                for: records.filter { $0.displayName.contains("CartId") || $0.displayName.contains("CartToken") }
            ) {}
        }
    }
    
    private func makeCookies(for webBaseUrl: String) -> [HTTPCookie] {
        let domain = webBaseUrl.replacingOccurrences(of: "https://", with: "")
        var cookies: [HTTPCookie] = []
        for cookie in self where !cookie.value.isEmpty {
            let httpKey = HTTPCookiePropertyKey(rawValue: cookie.key.rawValue)
            let cookieProps: [HTTPCookiePropertyKey: Any] = [
                .domain: "\(domain)",
                .path: "/",
                .name: httpKey,
                .value: cookie.value,
                .secure: "FALSE"
            ]
            guard let httpCookie = HTTPCookie(properties: cookieProps) else { return [] }
            cookies.append(httpCookie)
        }
        return cookies
    }
}

