//
//  ScriptMessageAction.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import UIKit

protocol ScriptMessageData {}

struct ScriptMessageAction {
    let type: ScriptMessageActionType
    let data: ScriptMessageData?
}
    
extension ScriptMessageAction: Decodable {
    private enum CodingKeys: String, CodingKey {
        case action
        case payload
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        type = try container.decode(ScriptMessageActionType.self, forKey: .action)
        switch type {
        case .openUrl:
            data = try container.decode(ScriptMessageUrlActionData.self, forKey: .payload)
        default:
            data = nil
        }
    }
}

enum ScriptMessageActionType: String, Decodable {
    case openUrl
    case close
    case login
    case unknown
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let type = try container.decode(String.self)
        self = ScriptMessageActionType(rawValue: type) ?? .unknown
    }

    init(type: String) {
        self = ScriptMessageActionType(rawValue: type) ?? .unknown
    }
}

struct ScriptMessageUrlActionData: ScriptMessageData, Decodable {
    let url: String?
}

