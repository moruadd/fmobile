//
//  ScriptMessageDecoder.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import Foundation
import WebKit

final class ScriptMessageDecoder {
    func decodeScriptMessage(message: WKScriptMessage) -> ScriptMessageAction? {
        guard let jsonData = try? JSONSerialization.data(withJSONObject: message.body, options: []),
              let jsonString = String(data: jsonData, encoding: .utf8),
              let data = jsonString.data(using: .utf8),
              let action = try? JSONDecoder().decode(ScriptMessageAction.self, from: data)
        else { return nil }
        return action
    }
}

