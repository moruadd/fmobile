//
//  WebViewControllerAppearance.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import UIKit

struct WebViewControllerAppearance {
    let navigationTitle: String
    let navigationBarTintColor: UIColor
    let navigationTitleTextAttributes: [NSAttributedString.Key: Any]
    let skeletonTintColor: UIColor
    
    init(navigationTitle: String = "",
         navigationBarTintColor: UIColor,
         navigationTitleTextAttributes: [NSAttributedString.Key: Any],
         skeletonTintColor: UIColor,
         rightBarButtonItemImage: UIImage? = nil) {
        self.navigationTitle = navigationTitle
        self.navigationBarTintColor = navigationBarTintColor
        self.navigationTitleTextAttributes = navigationTitleTextAttributes
        self.skeletonTintColor = skeletonTintColor
    }
}

