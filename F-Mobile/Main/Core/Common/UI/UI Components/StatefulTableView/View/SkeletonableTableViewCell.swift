//
//  SkeletonableTableViewCell.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import SkeletonView
import UIKit

private enum Constants {
    static let cornerRadius: Float = 4.0
}

class SkeletonableTableViewCell: UITableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        setupSkeletonableViews()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        hideSkeletonableAnimating()
    }

    /// Показать "Skeletonable" анимацию
    func showSkeletonableAnimating(usingColor color: UIColor = SkeletonAppearance.default.tintColor) {
        showAnimatedSkeleton(usingColor: color, animation: .none, transition: .none)
    }

    /// Скрыть  "Skeletonable" анимацию
    func hideSkeletonableAnimating() {
        hideSkeleton(reloadDataAfter: false, transition: .crossDissolve(0.35))
    }

    func setupSkeletonableViews() {
        var views = contentView.subviews
        views.append(self)
        views.forEach { view in
            view.isSkeletonable = true
            view.skeletonCornerRadius = Constants.cornerRadius
        }
    }
}

