//
//  BackgroundStateView.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import UIKit

struct BackgroundStateViewAppearance {
    struct LabelAppearance {
        var font: UIFont?
        var textColor: UIColor?

        init(font: UIFont? = nil, textColor: UIColor? = nil) {
            self.font = font
            self.textColor = textColor
        }
    }

    var imageTintColor: UIColor?
    var backgroundColor: UIColor?
    var titleAppearance: LabelAppearance
    var subtitleAppearance: LabelAppearance
    var buttonTitleAppearance: LabelAppearance

    init(imageTintColor: UIColor? = nil,
         backgroundColor: UIColor?,
         titleAppearance: LabelAppearance,
         subtitleAppearance: LabelAppearance,
         buttonTitleAppearance: LabelAppearance) {
        self.imageTintColor = imageTintColor
        self.backgroundColor = backgroundColor
        self.titleAppearance = titleAppearance
        self.subtitleAppearance = subtitleAppearance
        self.buttonTitleAppearance = buttonTitleAppearance
    }
}

final class BackgroundStateView: UIView, NibOwnerLoadable {
    var onActionDidTap: Callback?

    @IBOutlet private var imageView: UIImageView!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var subtitleLabel: UILabel!
    @IBOutlet private var actionButton: UIButton!

    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNibContent()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        loadNibContent()
    }

    @IBAction private func actionButtonDidTap(_ sender: UIButton) {
        onActionDidTap?()
    }
    
    func configure(with viewModel: BackgroundStateViewModel) {
        imageView.image = viewModel.image
        titleLabel.text = viewModel.title
        subtitleLabel.text = viewModel.subtitle
        actionButton.setTitle(viewModel.buttonTitle, for: .normal)
        actionButton.isHidden = viewModel.isButtonHidden
        setupAppearance(viewModel.appearance)
    }

    private func setupAppearance(_ appearance: BackgroundStateViewAppearance) {
        imageView.tintColor = appearance.imageTintColor
        setupLabelAppearance(titleLabel, appearance: appearance.titleAppearance)
        setupLabelAppearance(subtitleLabel, appearance: appearance.subtitleAppearance)
        actionButton.setTitleColor(appearance.buttonTitleAppearance.textColor, for: .normal)
        actionButton.titleLabel?.font = appearance.buttonTitleAppearance.font
    }

    private func setupLabelAppearance(_ label: UILabel, appearance: BackgroundStateViewAppearance.LabelAppearance) {
        label.font = appearance.font
        label.textColor = appearance.textColor
    }
}

