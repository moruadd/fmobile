//
//  RefreshControl.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import UIKit

protocol RefreshControlDelegate: AnyObject {
    func onDidRefresh()
}

final class RefreshControl: UIRefreshControl {
    
    weak var delegate: RefreshControlDelegate?

    override var isHidden: Bool {
        get {
            return super.isHidden
        }
        set(hiding) {
            if hiding {
                guard frame.origin.y >= 0 else { return }
                super.isHidden = hiding
            } else {
                guard frame.origin.y < 0 else { return }
                super.isHidden = hiding
            }
        }
    }

    override var frame: CGRect {
        didSet {
            if frame.origin.y < 0 {
                isHidden = false
            } else {
                isHidden = true
            }
        }
    }
    
    override init() {
        super.init()
        addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let originalFrame = frame
        frame = originalFrame
    }
    
    @objc private func handleRefresh(_ refreshControl: UIRefreshControl) {
        delegate?.onDidRefresh()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.endRefreshing()
        }
    }
}
