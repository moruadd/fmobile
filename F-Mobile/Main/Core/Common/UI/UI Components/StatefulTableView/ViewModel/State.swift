//
//  State.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import UIKit

class State {
    weak var tableView: StatefulTableView!

    init(tableView: StatefulTableView) {
        self.tableView = tableView
    }

    static func state(_ state: Kind, tableView: StatefulTableView) -> State {
        switch state {
        case let .loaded(animated):
            return LoadedState(animated: animated, tableView: tableView)
        case .loading:
            return LoadingState(tableView: tableView)
        case let .empty(viewModel, action):
            return EmptyState(viewModel: viewModel, action: action, tableView: tableView)
        case let .error(viewModel, action):
            return ErrorState(viewModel: viewModel, action: action, tableView: tableView)
        }
    }

    func enter() {}
}

extension State {
    enum Kind {
        case loading
        case loaded(animated: Bool)
        case empty(viewModel: BackgroundStateViewModel, action: Callback?)
        case error(viewModel: BackgroundStateViewModel, action: Callback?)
    }
}

final class LoadedState: State {
    let animated: Bool
    
    init(animated: Bool, tableView: StatefulTableView) {
        self.animated = animated
        super.init(tableView: tableView)
    }
    
    override func enter() {
        super.enter()
        tableView.stopSkeletonableAnimating(animated: animated)
        tableView.removeStateView()
    }
}

final class LoadingState: State {
    override func enter() {
        tableView.removeStateView()
        tableView.startSkeletonableAnimating()
    }
}

final class EmptyState: State {
    private var viewModel: BackgroundStateViewModel
    var action: Callback?

    init(viewModel: BackgroundStateViewModel, action: Callback?, tableView: StatefulTableView) {
        self.viewModel = viewModel
        self.action = action
        super.init(tableView: tableView)
    }

    override func enter() {
        tableView.stopSkeletonableAnimating(animated: false, refreshAnimated: false)
        tableView.showStateView(viewModel: viewModel, action: action)
    }
}

final class ErrorState: State {
    private var viewModel: BackgroundStateViewModel
    var action: Callback?

    init(viewModel: BackgroundStateViewModel, action: Callback?, tableView: StatefulTableView) {
        self.viewModel = viewModel
        self.action = action
        super.init(tableView: tableView)
    }

    override func enter() {
        tableView.stopSkeletonableAnimating(animated: false, refreshAnimated: false)
        tableView.showStateView(viewModel: viewModel, action: action)
    }
}

