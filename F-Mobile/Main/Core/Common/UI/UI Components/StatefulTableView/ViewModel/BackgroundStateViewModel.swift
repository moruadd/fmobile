//
//  BackgroundStateViewModel.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import UIKit

struct BackgroundStateViewModel {
    var image: UIImage?
    var title: String
    var subtitle: String
    var buttonTitle: String
    var appearance: BackgroundStateViewAppearance
    var isButtonHidden: Bool

    init(image: UIImage?,
         title: String,
         subtitle: String,
         buttonTitle: String,
         appearance: BackgroundStateViewAppearance,
         isButtonHidden: Bool = false) {
        self.image = image
        self.title = title
        self.subtitle = subtitle
        self.buttonTitle = buttonTitle
        self.appearance = appearance
        self.isButtonHidden = isButtonHidden
    }
}

