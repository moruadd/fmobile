//
//  StatefulTableView.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import SkeletonView
import UIKit

private enum Constants {
    static let cornerRadius = 4
    static let animationDuration = 0.3
}

class StatefulTableView: UITableView {
    var onPullToRefresh: Callback?
    private var state: State?
    private var isLoading = false

    private let stateContainerView = UIView()
    private let stateView = BackgroundStateView()
    
    private lazy var pullToRefreshControl: RefreshControl = {
        let refreshControl: RefreshControl = .init()
        refreshControl.delegate = self
        return refreshControl
    }()

    @IBInspectable var skeletonTintColor: UIColor = .lightGray {
        didSet {
            SkeletonAppearance.default.tintColor = skeletonTintColor
        }
    }

    @IBInspectable var multilineCornerRadius: Int = Constants.cornerRadius {
        didSet {
            SkeletonAppearance.default.multilineCornerRadius = multilineCornerRadius
        }
    }

    @IBInspectable var isPullToRefreshEnabled: Bool = false {
        didSet {
            setupRefreshControl()
        }
    }

    @IBInspectable var refreshControlTintColor: UIColor = .white {
        didSet {
            pullToRefreshControl.tintColor = refreshControlTintColor
        }
    }

    @IBInspectable var refreshControlBackgroundColor: UIColor = .black {
        didSet {
            pullToRefreshControl.backgroundColor = refreshControlBackgroundColor
        }
    }

    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        setup()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        setupStateView()
    }

    /// Показ "Skeletonable" анимации на всех видимых ячейках
    func startSkeletonableAnimating() {
        guard !isLoading else { return }
        reloadData()
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.startAnimatingVisibleCells()
            self.isLoading = true
        }
    }

    /// Прекращение "Skeletonable" анимации
    func stopSkeletonableAnimating(animated: Bool = true, refreshAnimated: Bool = true) {
        isLoading = false
        stopAnimatingVisibleCells()
        reloadData()
        guard animated else { return }
        beginUpdates()
        endUpdates()
    }

    func setState(_ kind: State.Kind) {
        state = .state(kind, tableView: self)
        state?.enter()
    }

    private func startAnimatingVisibleCells() {
        isScrollEnabled = false
        for cell in visibleCells {
            cell.isUserInteractionEnabled = false
            if let skeletonableCell = cell as? SkeletonableTableViewCell {
                skeletonableCell.showSkeletonableAnimating(usingColor: skeletonTintColor)
            } else {
                cell.showAnimatedSkeleton(usingColor: skeletonTintColor, animation: .none, transition: .none)
            }
        }
    }

    private func stopAnimatingVisibleCells() {
        isScrollEnabled = true
        for cell in visibleCells {
            cell.isUserInteractionEnabled = true
            if let skeletonableCell = cell as? SkeletonableTableViewCell {
                skeletonableCell.hideSkeletonableAnimating()
            } else {
                cell.hideSkeleton()
            }
        }
    }

    private func setup() {
        tableFooterView = UIView()
        isSkeletonable = true
        SkeletonAppearance.default.multilineCornerRadius = Constants.cornerRadius
        setupRefreshControl()
        addEmptyFooter()
    }

    private func setupRefreshControl() {
        if isPullToRefreshEnabled {
            self.refreshControl = pullToRefreshControl
            backgroundColor = .clear
        } else {
            self.refreshControl = nil
        }
    }
    
    private func setupStateView() {
        stateContainerView.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height)
        stateView.translatesAutoresizingMaskIntoConstraints = false
        stateContainerView.addSubview(stateView)
        stateView.widthAnchor.constraint(equalToConstant: bounds.size.width).isActive = true
        stateView.setCenterConstaints(by: stateContainerView)
    }
}

extension StatefulTableView {
    func showStateView(viewModel: BackgroundStateViewModel, action: Callback?) {
        stateView.onActionDidTap = {
            action?()
        }
        stateView.configure(with: viewModel)
        stateContainerView.backgroundColor = viewModel.appearance.backgroundColor
        stateContainerView.alpha = 0
        backgroundView = stateContainerView
        isScrollEnabled = false
        UIView.animate(withDuration: Constants.animationDuration) { [weak self] in
            self?.stateContainerView.alpha = 1
        }
    }

    func removeStateView() {
        stateContainerView.alpha = 0
        setupRefreshControl()
        isScrollEnabled = true
    }
}

// MARK: - RefreshControlDelegate

extension StatefulTableView: RefreshControlDelegate {
    
    func onDidRefresh() {
        onPullToRefresh?()
    }
}

