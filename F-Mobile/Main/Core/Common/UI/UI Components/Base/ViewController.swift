//
//  ViewController.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import UIKit

extension Notification.Name {
    static let didLanguageChange = Notification.Name("didLanguageChange")
    static let didUserChange = Notification.Name("didUserChange")
    static let didChangeEnvironment = Notification.Name("didChangeEnvironment")
    static let didTokenExpired = Notification.Name("didTokenExpired")
}

class ViewController: UIViewController, CoordinatorNavigationControllerDelegate {
    override var prefersStatusBarHidden: Bool { false }

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(localize), name: .didLanguageChange, object: nil)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupCoordinatorNavigationController()
    }

    private func setupCoordinatorNavigationController() {
        guard let navigationController = navigationController as? CoordinatorNavigationController else { return }
        navigationController.coordinatorNavigationDelegate = self
    }

    func transitionBackDidFinish() {}
    func customBackButtonDidTap() {}
    func customCloseButtonDidTap() {}

    func updateLocalization() {}
    func refreshRequests() {}

    @objc
    private func localize() {
        updateLocalization()
        refreshRequests()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

