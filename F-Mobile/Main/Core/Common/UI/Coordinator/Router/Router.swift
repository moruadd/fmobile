//
//  Router.swift
//  F-Mobile
//
//  Created by Damir on 23.09.2023.
//

import UIKit

protocol Router: Presentable {
    var rootController: CoordinatorNavigationController? { get }
    
    func present(_ module: Presentable?, animated: Bool, isCloseable: Bool)
    // swiftlint:disable function_parameter_count multiline_parameters
    func present(_ module: Presentable?, animated: Bool,
                 modalPresentationStyle: UIModalPresentationStyle,
                 modalTransitionStyle: UIModalTransitionStyle,
                 isCloseable: Bool,
                 modalPresentationCapturesStatusBarAppearance: Bool)
    func dismissModule(animated: Bool, completion: (() -> Void)?)
    func dismissPresentedModule(_ presentedModule: Presentable?, animated: Bool, completion: Callback?)
    func push(_ module: Presentable?, animated: Bool, hideBottomBarWhenPushed: Bool, completion: Callback?)
    func popModule(animated: Bool)
    func popToModule(_ module: Presentable?, animated: Bool)
    func popToRootModule(animated: Bool)
    func runCompletion(for controller: UIViewController)
    func presentInSlidingCard(_ module: Presentable?, animated: Bool)
    func presentAdditionalMenu(_ module: Presentable, originFrame: CGRect)
    func presentStories(_ module: Presentable?, animated: Bool, originFrame: CGRect)
    func presentDropdown(_ module: DropdownPresentable?)
    func setRootModule(_ module: Presentable?, isNavigationBarHidden: Bool)
}

extension Router {
    
    func present(_ module: Presentable?, animated: Bool = true, isCloseable: Bool = false) {
        present(module, animated: animated, isCloseable: isCloseable)
    }
    
    // swiftlint:disable vertical_parameter_alignment multiline_arguments
    func present(_ module: Presentable?,
                        animated: Bool,
                        modalPresentationStyle: UIModalPresentationStyle,
                        modalTransitionStyle: UIModalTransitionStyle = .coverVertical,
                        isCloseable: Bool = false,
                        modalPresentationCapturesStatusBarAppearance: Bool = false) {
        present(module, animated: animated,
                modalPresentationStyle: modalPresentationStyle,
                modalTransitionStyle: modalTransitionStyle,
                isCloseable: isCloseable,
                modalPresentationCapturesStatusBarAppearance: modalPresentationCapturesStatusBarAppearance)
    }

    func dismissModule(animated: Bool = true, completion: Callback? = nil) {
        dismissModule(animated: animated, completion: completion)
    }
    
    func dismissPresentedModule(_ presentedModule: Presentable?, animated: Bool = true, completion: Callback? = nil) {
        dismissPresentedModule(presentedModule, animated: animated, completion: completion)
    }
    
    func push(_ module: Presentable?, animated: Bool = true, hideBottomBarWhenPushed: Bool = true, completion: Callback? = nil) {
        push(module, animated: animated, hideBottomBarWhenPushed: hideBottomBarWhenPushed, completion: completion)
    }
    
    func popModule(animated: Bool = true) {
        popModule(animated: animated)
    }

    func setRootModule(_ module: Presentable?, isNavigationBarHidden: Bool = false) {
        setRootModule(module, isNavigationBarHidden: isNavigationBarHidden)
    }
    
    func popToModule(_ module: Presentable?, animated: Bool = true) {
        popToModule(module, animated: animated)
    }
    
    func popToRootModule(animated: Bool = true) {
        popToRootModule(animated: animated)
    }
    
    
}

final class RouterImpl: Router {
    
    private static var storiesTransitioningDelegate = StoriesTransitionDelegate()
    private static var slidingTransitioningDelegate = SlidingTransitioningDelegate()
    private static var dropdownTransitioningDelegate = DropdownTransitioningDelegate()
    private static var additionalMenuTransitioningDelegate = AdditionalMenuTransitioningDelegate()
    
    private var completions: [UIViewController: Callback]
    
    private(set) weak var rootController: CoordinatorNavigationController?
    
    init(rootController: CoordinatorNavigationController) {
        self.rootController = rootController
        completions = [:]
    }
    
    func toPresent() -> UIViewController? {
        rootController
    }
    
    func present(_ module: Presentable?, animated: Bool, isCloseable: Bool) {
        if #available(iOS 13.0, *) {
            present(module, animated: animated, modalPresentationStyle: .automatic,
                    modalTransitionStyle: .coverVertical,
                    isCloseable: isCloseable, modalPresentationCapturesStatusBarAppearance: false)
        } else {
            present(module, animated: animated, modalPresentationStyle: .overFullScreen,
                    modalTransitionStyle: .coverVertical,
                    isCloseable: isCloseable, modalPresentationCapturesStatusBarAppearance: false)
        }
    }
    
    func present(_ module: Presentable?,
                        animated: Bool,
                        modalPresentationStyle: UIModalPresentationStyle,
                        modalTransitionStyle: UIModalTransitionStyle,
                        isCloseable: Bool,
                        modalPresentationCapturesStatusBarAppearance: Bool) {
        guard let controller = module?.toPresent() else { return }
        controller.modalTransitionStyle = modalTransitionStyle
        controller.modalPresentationCapturesStatusBarAppearance = modalPresentationCapturesStatusBarAppearance
        controller.modalPresentationStyle = modalPresentationStyle
        if #available(iOS 13.0, *) { controller.isModalInPresentation = true }
        if isCloseable {
            rootController?.presentWithCloseBarButtonItem(controller,
                                                          animated: animated,
                                                          modalPresentationStyle: modalPresentationStyle,
                                                          completion: nil)
        } else {
            rootController?.present(controller, animated: animated, completion: nil)
        }
    }
    
    func dismissModule(animated: Bool, completion: (() -> Void)?) {
        rootController?.dismiss(animated: animated, completion: completion)
    }
    
    /// Используется, чтобы скрыть контроллер презентованным на презентованном модуле
    func dismissPresentedModule(_ presentedModule: Presentable?, animated: Bool, completion: (() -> Void)?) {
        guard let controller = presentedModule?.toPresent() else { return }
        controller.dismiss(animated: true, completion: completion)
    }
    
    func push(_ module: Presentable?, animated: Bool, hideBottomBarWhenPushed: Bool, completion: (() -> Void)?) {
        guard let controller = module?.toPresent(), controller is UINavigationController == false else {
            assertionFailure("Deprecated push UINavigationController.")
            return
        }
        if let completion = completion {
            completions[controller] = completion
        }
        rootController?.enableSwipeBack()
        controller.hidesBottomBarWhenPushed = hideBottomBarWhenPushed
        rootController?.pushViewController(controller, animated: animated)
    }
    
    func popModule(animated: Bool) {
        if let controller = rootController?.popViewController(animated: animated) {
            runCompletion(for: controller)
        }
    }
    
    func setRootModule(_ module: Presentable?, isNavigationBarHidden: Bool) {
        guard let controller = module?.toPresent() else { return }
        rootController?.setViewControllers([controller], animated: false)
        rootController?.isNavigationBarHidden = isNavigationBarHidden
    }
    
    func popToModule(_ module: Presentable?, animated: Bool) {
        guard let controller = module?.toPresent() else { return }
        if let controllers = rootController?.popToViewController(controller, animated: animated) {
            controllers.forEach { runCompletion(for: $0) }
        }
    }
    
    func popToRootModule(animated: Bool) {
        if let controllers = rootController?.popToRootViewController(animated: animated) {
            controllers.forEach { runCompletion(for: $0) }
        }
    }
    
    func runCompletion(for controller: UIViewController) {
        guard let completion = completions[controller] else { return }
        completion()
        completions.removeValue(forKey: controller)
    }
    
    func presentStories(_ module: Presentable?, animated: Bool, originFrame: CGRect) {
        guard let controller = module?.toPresent() else { return }
        RouterImpl.storiesTransitioningDelegate.originFrame = originFrame
        controller.transitioningDelegate = RouterImpl.storiesTransitioningDelegate
        controller.modalPresentationCapturesStatusBarAppearance = true
        present(module, animated: animated, modalPresentationStyle: .custom,
                modalTransitionStyle: .coverVertical,
                isCloseable: false, modalPresentationCapturesStatusBarAppearance: false)
    }
    
    func presentInSlidingCard(_ module: Presentable?, animated: Bool) {
        guard let controller = module?.toPresent() else { return }
        controller.transitioningDelegate = RouterImpl.slidingTransitioningDelegate
        present(module, animated: animated, modalPresentationStyle: .custom,
                modalTransitionStyle: .coverVertical,
                isCloseable: false, modalPresentationCapturesStatusBarAppearance: false)
    }
    
    func presentDropdown(_ module: DropdownPresentable?) {
        guard let controller = module?.toPresent() else { return }
        controller.transitioningDelegate = RouterImpl.dropdownTransitioningDelegate
        present(module,
                animated: true,
                modalPresentationStyle: .custom,
                modalTransitionStyle: .coverVertical,
                isCloseable: false,
                modalPresentationCapturesStatusBarAppearance: false)
        
    }
    
    func presentAdditionalMenu(_ module: Presentable, originFrame: CGRect) {
        guard let controller = module.toPresent() else { return }
        RouterImpl.additionalMenuTransitioningDelegate.originFrame = originFrame
        controller.transitioningDelegate = RouterImpl.additionalMenuTransitioningDelegate
        present(module, animated: true, modalPresentationStyle: .custom,
                modalTransitionStyle: .coverVertical,
                isCloseable: false, modalPresentationCapturesStatusBarAppearance: false)
        
    }
    
    
}

