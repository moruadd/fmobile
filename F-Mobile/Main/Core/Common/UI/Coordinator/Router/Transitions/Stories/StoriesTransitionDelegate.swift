//
//  StoriesTransitionDelegate.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import UIKit

final class StoriesTransitionDelegate: NSObject, UIViewControllerTransitioningDelegate {
    var originFrame: CGRect?

    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        guard let originFrame = originFrame else { return nil }
        return StoriesPresentAnimationController(originFrame: originFrame)
    }

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        guard let originFrame = originFrame else { return nil }
        return StoriesDismissAnimationController(destinationFrame: originFrame)
    }
}

