//
//  SlidingTransitioningDelegate.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import UIKit

final class SlidingTransitioningDelegate: NSObject, UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController,
                                presenting: UIViewController?,
                                source: UIViewController) -> UIPresentationController? {
        SlidingPresentationController(presentedViewController: presented, presenting: presenting)
    }
}

