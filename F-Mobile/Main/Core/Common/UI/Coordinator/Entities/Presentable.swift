//
//  Presentable.swift
//  F-Mobile
//
//  Created by Damir on 23.09.2023.
//

import UIKit

protocol Presentable {
    func toPresent() -> UIViewController?
}

extension UIViewController: Presentable {
    func toPresent() -> UIViewController? {
        self
    }
}

