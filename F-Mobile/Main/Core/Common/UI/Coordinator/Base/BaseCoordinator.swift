//
//  BaseCoordinator.swift
//  F-Mobile
//
//  Created by Damir on 23.09.2023.
//

import UIKit
import Fundamental

class BaseCoordinator: Coordinator {
    var childCoordinators: [Coordinator] = []
    
    private(set) var baseChildCoordinators: [Coordinator] = []
    let router: Router

    init(router: Router) {
        self.router = router
    }

    func start() {}
//    func start(with action: DeepLinkAction?) {}
    
    func dismiss(child coordinator: Coordinator?) {
        router.dismissModule(animated: true, completion: nil)
        removeDependency(coordinator)
    }

    func addDependency(_ coordinator: Coordinator) {
        guard !baseChildCoordinators.contains(where: { $0 === coordinator }) else { return }
        baseChildCoordinators.append(coordinator)
    }

    func removeDependency(_ coordinator: Coordinator?) {
        guard !baseChildCoordinators.isEmpty,
            let coordinator = coordinator else { return }
        if let coordinator = coordinator as? BaseCoordinator, !coordinator.baseChildCoordinators.isEmpty {
            coordinator.baseChildCoordinators.filter { $0 !== coordinator }.forEach { coordinator.removeDependency($0) }
        }
        baseChildCoordinators.removeAll { $0 === coordinator }
    }

    func clearChildCoordinators() {
        baseChildCoordinators.forEach { removeDependency($0) }
    }
}

