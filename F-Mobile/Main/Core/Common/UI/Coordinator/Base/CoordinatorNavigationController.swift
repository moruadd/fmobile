//
//  CoordinatorNavigationController.swift
//  F-Mobile
//
//  Created by Damir on 23.09.2023.
//

import UIKit

protocol CoordinatorNavigationControllerDelegate: AnyObject {
    func transitionBackDidFinish()
    func customBackButtonDidTap()
    func customCloseButtonDidTap()
}

class CoordinatorNavigationController: UINavigationController {
    weak var coordinatorNavigationDelegate: CoordinatorNavigationControllerDelegate?

    private let backBarButtonImage: UIImage?
    private let closeBarButtonImage: UIImage?
    private var isPushBeingAnimated = false

    init(backBarButtonImage: UIImage?, closeBarButtonImage: UIImage? = nil) {
        self.backBarButtonImage = backBarButtonImage
        self.closeBarButtonImage = closeBarButtonImage
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        nil
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        view.backgroundColor = .white
        if #available(iOS 13.0, *) {
            self.navigationBar.scrollEdgeAppearance = navigationBar.scrollEdgeAppearance
        } else {
            // Fallback on earlier versions
        }
    }

    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        isPushBeingAnimated = true
        super.pushViewController(viewController, animated: animated)
        setupBackButton(viewController: viewController)
    }

    func presentWithCloseBarButtonItem(_ viewController: UIViewController,
                                       animated: Bool,
                                       modalPresentationStyle: UIModalPresentationStyle,
                                       completion: Callback?) {
        let rootController = CloseableViewController(backBarButtonImage: backBarButtonImage, closeBarButtonImage: closeBarButtonImage)
        rootController.modalPresentationStyle = modalPresentationStyle
        if #available(iOS 13.0, *) { rootController.isModalInPresentation = true }
        rootController.setViewControllers([viewController], animated: true)
        present(rootController, animated: animated, completion: completion)
    }

    func setupBackButton(viewController: UIViewController) {
        viewController.navigationItem.hidesBackButton = true
        viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(image: backBarButtonImage,
                                                                          style: .done,
                                                                          target: self,
                                                                          action: #selector(backButtonDidTap))
    }

    func enableSwipeBack() {
        interactivePopGestureRecognizer?.isEnabled = true
        interactivePopGestureRecognizer?.delegate = nil
    }

    @objc
    private func backButtonDidTap() {
        coordinatorNavigationDelegate?.customBackButtonDidTap()
    }
}

extension CoordinatorNavigationController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        guard let coordinator = navigationController.topViewController?.transitionCoordinator else { return }
        coordinator.notifyWhenInteractionChanges { [weak self] context in
            guard !context.isCancelled else { return }
            self?.coordinatorNavigationDelegate?.transitionBackDidFinish()
        }
    }

    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        guard let swipeNavigationController = navigationController as? CoordinatorNavigationController else { return }
        swipeNavigationController.isPushBeingAnimated = false
    }
}

