//
//  PropertyFormatter.swift
//  F-Mobile
//
//  Created by Damir on 30.10.2023.
//

import Foundation

final class PropertyFormatter {
    let appLanguage: AppLanguage
    var formatters: [String: Any] = [:]
    let cachedFormattersQueue = DispatchQueue(label: "team.alabs.formatter.queue")

    init(appLanguage: AppLanguage) {
        self.appLanguage = appLanguage
    }
}

