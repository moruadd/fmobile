//
//  PhoneNumberFormatting.swift
//  F-Mobile
//
//  Created by Damir on 30.10.2023.
//

import Foundation

protocol PhoneNumberFormatting: AnyObject {
    func formattedPhoneNumber(from text: String) -> (value: String?, cursorIndex: Int)
    func rawPhoneNumber(from text: String) -> String?
}

private enum Constants {
    static let digitsCount = 10
    static let codePrefix = "+7"
    static let possiblePrefixes = ["8", codePrefix]
}

extension PropertyFormatter: PhoneNumberFormatting {
    /// Форматирует телефонный номер в формат и возвращает индекс курсора:
    /// +7 (XXX) XXX-XX-XX
    func formattedPhoneNumber(from text: String) -> (value: String?, cursorIndex: Int) {
        let digits = text.unicodeScalars
            .filter { CharacterSet.decimalDigits.contains($0) }
            .map { String($0) }

        guard digits.count <= Constants.digitsCount else { return (nil, Constants.digitsCount) }

        var formattedString = Constants.codePrefix
        for (index, digit) in digits.enumerated() {
            switch index {
            case 0:
                formattedString += " (\(digit)"
            case 2:
                formattedString += "\(digit)) "
            case 6, 8:
                formattedString += " \(digit)"
            default:
                formattedString += digit
            }
        }
        let position = formattedString.count
        for index in digits.count ..< Constants.digitsCount {
            switch index {
            case 0:
                formattedString += " (_"
            case 2:
                formattedString += "_) "
            case 6, 8:
                formattedString += " _"
            default:
                formattedString += "_"
            }
        }
        return (formattedString, position)
    }

    /// Конвертирует телефонный номер в API-формат(10-значный, только цифры)
    /// Пример: 7076420271
    func rawPhoneNumber(from text: String) -> String? {
        var formatted = ""
        for pref in Constants.possiblePrefixes where text.prefix(2).hasPrefix(pref) {
            formatted = text.replacingOccurrences(
                of: pref,
                with: "",
                range: text.range(of: pref)
            )
            break
        }
        let digits = formatted
            .unicodeScalars
            .filter { CharacterSet.decimalDigits.contains($0) }
            .map { String($0) }
            .joined()
        guard digits.count <= Constants.digitsCount else { return nil }
        return digits
    }
}

