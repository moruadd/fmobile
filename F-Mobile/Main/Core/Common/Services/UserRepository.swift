//
//  UserRepository.swift
//  F-Mobile
//
//  Created by Damir on 30.10.2023.
//

import Promises

final class UserRepository {
    typealias Provider = RegularAccountProvider
    
    private let user: User
    private let userSessionStorage: UserSessionStorage
    private var provider: Provider?
    
    init(user: User, userSessionStorage: UserSessionStorage) {
        self.user = user
        self.userSessionStorage = userSessionStorage
    }
    
    func setProvider(_ provider: Provider) {
        self.provider = provider
    }
    
    @discardableResult
    func actualize() -> Promise<Void> {
        guard let provider = provider else { return Promise(()) }
        switch user.type {
        case let .regular(regularUser):
            return provider.getRegularAccountProfile()
                .then { [weak userSessionStorage] accountProfile in
                    regularUser.accountProfile = accountProfile
                    userSessionStorage?.save(phoneNumber: accountProfile.phoneNumber)
                }
                .voided()
        case .eshop(_):
            return Promise(())
        }
    }
}

