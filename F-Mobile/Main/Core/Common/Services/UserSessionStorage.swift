//
//  UserSessionStorage.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import AlabsNetwork
import Fundamental

final class UserSessionStorage {
    
    // MARK: - Public Properties
    
    var accessToken: String? {
        return tokenStorage.getAccessToken()
    }

    var refreshToken: String? {
        return tokenStorage.getRefreshToken()
    }
    
    var phoneNumber: String? {
        return userStorage.getPhoneNumber()
    }
    
    @KeychainEntry("qrPaymentToken")
    var qrPaymentToken: String?

    @KeychainEntry("pin")
    var pin: String?

    @KeychainEntry("otpId")
    var otpId: String?
    
    @KeychainEntry("subscriberId")
    var subscriberId: String?

    @UserDefaultsEntry("isBiometricAuthBeingUsed", defaultValue: false)
    var isBiometricAuthBeingUsed: Bool
    
    @UserDefaultsEntry("isInNewRoamingCountry", defaultValue: false)
    var isInNewRoamingCountry: Bool
    
    @UserDefaultsOptionalEntry("lastVisitedCountryCode")
    var lastVisitedCountryCode: String?
    
    @UserDefaultsEntry("isEshop", defaultValue: false)
    var isEshop: Bool

    // MARK: - Dependency
    
    private let tokenStorage: TokenStorage
    private let userStorage: UserStorage
    
    // MARK: - Init
    
    init(tokenStorage: TokenStorage, userStorage: UserStorage) {
        self.tokenStorage = tokenStorage
        self.userStorage = userStorage
    }
    
    convenience init() {
        self.init(tokenStorage: TokenStorageImpl.shared, userStorage: UserStorageImpl.shared)
    }

    func clearAll() {
        qrPaymentToken = nil
        pin = nil
        otpId = nil
        subscriberId = nil
        isBiometricAuthBeingUsed = false
        tokenStorage.clean()
        userStorage.clean()
    }
 
    func save(accessToken: String, refreshToken: String) {
        tokenStorage.set(accessToken, refreshToken: refreshToken)
    }
    
    func save(phoneNumber: String?) {
        userStorage.set(phoneNumber)
    }
}

extension UserSessionStorage: NetworkAuthCredentialsStorable {
    
    func save(credentials: NetworkAuthCredentials) {
        tokenStorage.set(credentials.accessToken, refreshToken: credentials.refreshToken)
    }
}

