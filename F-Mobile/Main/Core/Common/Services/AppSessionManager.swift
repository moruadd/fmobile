//
//  AppSessionManager.swift
//  F-Mobile
//
//  Created by Damir on 26.09.2023.
//

import UIKit
import AlabsNetwork

private enum Constants {
    static let sessionTimeout: TimeInterval = 900
}

final class AppSessionManager {
    var onSessionExpired: Callback?

    @UserDefaultsEntry("isFirstLaunch", defaultValue: true)
    var isFirstLaunch: Bool
    
    @UserDefaultsEntry("isRoamingTutorialShown", defaultValue: false)
    var isRoamingTutorialShown: Bool
        
    @UserDefaultsOptionalEntry("isPushNotificationsEnabled")
    var isPushNotificationsEnabled: Bool?
    
    private var backgroundEnterTime: Date?
    private let notificationCenter: NotificationCenter

    init(notificationCenter: NotificationCenter) {
        self.notificationCenter = notificationCenter
        setupObservers()
    }

    @objc
    private func checkSessionTimeout() {
        let currentTime = Date()
        guard
            let backgroundEnterTime = backgroundEnterTime,
            currentTime.timeIntervalSince(backgroundEnterTime) > Constants.sessionTimeout else { return }
        onSessionExpired?()
    }

    private func setupObservers() {
        notificationCenter.addObserver(self,
                                       selector: #selector(updateBackgroundEnterTime),
                                       name: UIApplication.didEnterBackgroundNotification,
                                       object: nil)
        notificationCenter.addObserver(self,
                                       selector: #selector(checkSessionTimeout),
                                       name: UIApplication.willEnterForegroundNotification,
                                       object: nil)
    }

    @objc
    private func updateBackgroundEnterTime() {
        backgroundEnterTime = Date()
    }
}

