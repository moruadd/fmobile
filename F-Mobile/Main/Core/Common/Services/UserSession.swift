//
//  UserSession.swift
//  F-Mobile
//
//  Created by Damir on 26.09.2023.
//

extension TokenResponse: UserSessionCredentials {}

protocol UserSessionCredentials {
    var accessToken: String { get }
    var refreshToken: String { get }
    var subscriberId: UInt { get }
}

enum UserPinState: Equatable {
    case created(pin: String)
    case declined
    case notSet
}

protocol UserSessionDelegate: AnyObject {
    func didUpdate(subscriberId: String)
}

final class UserSession {
    let storage: UserSessionStorage
    
    weak var delegate: UserSessionDelegate?

    var pinState: UserPinState {
        get {
            switch storage.pin {
            case let .some(pin) where pin.isEmpty:
                return .declined
            case let .some(pin):
                return .created(pin: pin)
            case .none:
                return .notSet
            }
        }
        set {
            switch newValue {
            case let .created(pin):
                storage.pin = pin
            case .declined:
                storage.pin = ""
            case .notSet:
                storage.pin = nil
            }
        }
    }

    var otpId: String? {
        get {
            storage.otpId
        }
        set {
            let valueToSet = newValue != nil ? newValue : storage.otpId
            storage.otpId = valueToSet
        }
    }
    
    var subscriberId: String? {
        get {
            storage.subscriberId
        }
        set {
            if let id = newValue {
                delegate?.didUpdate(subscriberId: id)
            }
            storage.subscriberId = newValue
        }
    }

    var isBiometricAuthBeingUsed: Bool {
        get {
            storage.isBiometricAuthBeingUsed
        }
        set {
            storage.isBiometricAuthBeingUsed = newValue
        }
    }
    
    var isInNewRoamingCountry: Bool {
        get {
            storage.isInNewRoamingCountry
        }
        set {
            storage.isInNewRoamingCountry = newValue
        }
    }
    
    var lastVisitedCountryCode: String? {
        get {
            storage.lastVisitedCountryCode
        }
        set {
            storage.lastVisitedCountryCode = newValue
        }
    }

    init(storage: UserSessionStorage) {
        self.storage = storage
    }

    func start(with credentials: UserSessionCredentials, phoneNumber: String?) {
        storage.save(accessToken: credentials.accessToken, refreshToken: credentials.refreshToken)
        storage.save(phoneNumber: phoneNumber)
        subscriberId = String(credentials.subscriberId)
    }

    func finish() {
        storage.clearAll()
    }
}

