//
//  EshopUser.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import Foundation

final class EshopUser {
    var accountProfile: EshopAccountProfile?
    
    var contactPhone: String? {
        accountProfile?.contactPhone
    }
    
    init(accountProfile: EshopAccountProfile? = nil) {
        self.accountProfile = accountProfile
    }
        
    func reset() {
        accountProfile = nil
    }
}

