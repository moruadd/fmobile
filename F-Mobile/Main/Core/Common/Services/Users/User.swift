//
//  User.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import Promises

class User {
    enum `Type` {
        case regular(RegularUser)
        case eshop(EshopUser)
    }

    var type: Type = .regular(RegularUser())
    
    var msisdn: String? {
        switch type {
        case let .regular(user):
            return user.account?.msisdn
        case let .eshop(user):
            return user.accountProfile?.msisdn
        }
    }
    
    init() {}
    
    func reset() {
        switch type {
        case let .regular(user):
            user.account = nil
            user.accountProfile = nil
        case let .eshop(user):
            user.accountProfile = nil
        }
    }
}

