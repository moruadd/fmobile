//
//  RegularUser.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import Foundation
import Promises

private enum Constants {
    static let digitsCount = 10
}

class RegularUser {
    var account: RegularAccount?
    var accountProfile: RegularAccountProfile?

    var name: String {
        guard let account = account else { return "" }
        return account.name
    }

    var caption: String {
        guard let account = account else { return "" }
        return account.caption
    }

    var isMain: Bool {
        guard let account = account else { return true }
        return account.isMain
    }
    
    var msisdn: String? {
        account?.msisdn
    }
    
    var hasAdditionalPhoneNumber: Bool {
        return (additionalPhoneNumber ?? "").trimmingCharacters(in: CharacterSet.decimalDigits.inverted).count == Constants.digitsCount
    }
    
    var accounts: [RegularAccount] {
        guard let profile = accountProfile else { return [] }
        return profile.accounts
    }
    
    var isInRoaming: Bool {
        accountProfile?.isUserInRoaming ?? false
    }
    
    var country: Country? {
        accountProfile?.country
    }
    
    var tariffId: Int? {
        accountProfile?.tariff.id
    }
    
    var authEmail: String? {
        get { accountProfile?.authEmail }
        set { accountProfile?.authEmail = newValue }
    }
    
    var notificationEmail: String? {
        get { accountProfile?.notificationEmail }
        set { accountProfile?.notificationEmail = newValue }
    }

    var selectedRoamingCountry: RoamingCountryResponseModel? {
        didSet {
            isSelectedRoamingCountryChanged = selectedRoamingCountry != oldValue
        }
    }
    
    var additionalPhoneNumber: String? {
        get { accountProfile?.additionalPhoneNumber }
        set { accountProfile?.additionalPhoneNumber = newValue }
    }
    
    var isSelectedRoamingCountryChanged = false
    
    init(account: RegularAccount? = nil, accountProfile: RegularAccountProfile? = nil) {
        self.account = account
        self.accountProfile = accountProfile
    }
        
    func reset() {
        account = nil
    }
}

