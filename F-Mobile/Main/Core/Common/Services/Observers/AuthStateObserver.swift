//
//  AuthStateObserver.swift
//  F-Mobile
//
//  Created by Damir on 26.09.2023.
//

import UIKit
import AlabsNetwork
import Fundamental

final class AuthStateObserver: NetworkAuthStateObservable {
    
    private let user: User
    private let cache: InMemoryCache
    private let userSession: UserSession
    private let cookieCollection: CookieCollection
    
    private(set) weak var coordinator: (BaseCoordinator & ApplicationCoordinatorProtocol)?
    
    init(user: User,
         cache: InMemoryCache,
         userSession: UserSession,
         cookiesStorage: WebCookiesStorable) {
        self.user = user
        self.cache = cache
        self.userSession = userSession
        self.cookieCollection = cookiesStorage.cookieCollection
        NotificationCenter.default.addObserver(self, selector: #selector(tokenDidExpire), name: .didTokenExpired, object: nil)
    }

    @objc func tokenDidExpire() {
        forceLogout()
    }

    func forceLogout(completion: (() -> Void)? = nil) {
        configureUserSession()
        configureApplicationCoordinator(completion: completion)
        cookieCollection.clearCookies()
        cache.clear()
    }

    func setCoordinator(_ coordinator: (BaseCoordinator & ApplicationCoordinatorProtocol)?) {
        self.coordinator = coordinator
    }

    private func configureUserSession() {
        userSession.finish()
        user.reset()
    }

    private func configureApplicationCoordinator(completion: (() -> Void)? = nil) {
        coordinator?.clearChildCoordinators()
        coordinator?.router.dismissModule(animated: true, completion: nil)
        coordinator?.start()
        completion?()
    }
}

