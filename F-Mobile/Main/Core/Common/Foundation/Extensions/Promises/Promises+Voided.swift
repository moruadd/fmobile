//
//  Promises+Voided.swift
//  F-Mobile
//
//  Created by Damir on 30.10.2023.
//

import Promises

extension Promise {
    func voided() -> Promise<Void> {
        then { _ in
            Promise<Void>(Void())
        }
    }
}

