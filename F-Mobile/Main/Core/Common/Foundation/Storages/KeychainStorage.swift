//
//  KeychainStorage.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import KeychainAccess

private extension Keychain {
    static let `default` = Keychain()
}

@propertyWrapper
struct KeychainEntry {
    var wrappedValue: String? {
        get {
            try? Keychain.default.getString(key)
        }
        set {
            if let unwrappedValue = newValue {
                try? Keychain.default.set(unwrappedValue, key: key)
            } else {
                try? Keychain.default.remove(key)
            }
        }
    }

    private let key: String

    init(_ key: String) {
        self.key = key
    }
}

