//
//  UserDefaultsStorage.swift
//  F-Mobile
//
//  Created by Damir on 13.10.2023.
//

import Foundation

@propertyWrapper
struct UserDefaultsOptionalEntry<T> {
    var wrappedValue: T? {
        get {
            userDefaults.object(forKey: key) as? T
        }
        set {
            userDefaults.set(newValue, forKey: key)
        }
    }

    private let key: String
    private let userDefaults: UserDefaults

    init(_ key: String, userDefaults: UserDefaults = .standard) {
        self.key = key
        self.userDefaults = userDefaults
    }
}

@propertyWrapper
struct UserDefaultsOptionalEnumEntry<Enum: RawRepresentable> {
    var wrappedValue: Enum? {
        get { getWrappedValue() }
        set { updateWrappedValue(newValue) }
    }

    private let key: String
    private let defaultValue: Enum?

    init(_ key: String, defaultValue: Enum? = nil) {
        self.key = key
        self.defaultValue = defaultValue
    }

    private func getWrappedValue() -> Enum? {
        if let rawValue = UserDefaults.standard.object(forKey: key) as? Enum.RawValue {
            return Enum(rawValue: rawValue) ?? defaultValue
        } else {
            return defaultValue
        }
    }

    private func updateWrappedValue(_ newValue: Enum?) {
        if let newValue = newValue {
            UserDefaults.standard.set(newValue.rawValue, forKey: key)
        } else {
            UserDefaults.standard.removeObject(forKey: key)
        }
    }
}

@propertyWrapper
struct UserDefaultsEntry<T> {
    var wrappedValue: T {
        get {
            userDefaults.object(forKey: key) as? T ?? defaultValue
        }
        set {
            userDefaults.set(newValue, forKey: key)
        }
    }

    private let key: String
    private let defaultValue: T
    private let userDefaults: UserDefaults

    init(_ key: String, defaultValue: T, userDefaults: UserDefaults = .standard) {
        self.key = key
        self.defaultValue = defaultValue
        self.userDefaults = userDefaults
    }
}

