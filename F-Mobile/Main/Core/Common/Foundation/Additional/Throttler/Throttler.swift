//
//  Throttler.swift
//  F-Mobile
//
//  Created by Damir on 23.09.2023.
//

import Foundation
typealias Callback = () -> Void

final class Throttler {
    private var workItem = DispatchWorkItem {}
    private var previousRun = Date.distantPast
    private let queue: DispatchQueue
    private let minimumDelay: TimeInterval

    init(minimumDelay: TimeInterval = 0.5, queue: DispatchQueue = DispatchQueue.main) {
        self.minimumDelay = minimumDelay
        self.queue = queue
    }

    func throttle(_ handler: @escaping Callback) {
        workItem.cancel()

        workItem = DispatchWorkItem { [weak self] in
            self?.previousRun = Date()
            handler()
        }

        let delay = previousRun.timeIntervalSinceNow > minimumDelay ? 0.0 : minimumDelay
        queue.asyncAfter(deadline: .now() + delay, execute: workItem)
    }
}

