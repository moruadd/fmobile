//
//  DependencyContainerAssembly.swift
//  F-Mobile
//
//  Created by Damir on 26.09.2023.
//

import Swinject
import UIKit
import Fundamental
import AlabsNetwork

// swiftlint:disable all
private enum DependencyContextNames: String {
    case dp = "DigitalPlatform"
}

final class DependencyContainerAssembly: Assembly {
    func assemble(container: Container) {
        assembleSystemServices(container)
        assembleCustomServices(container)
        assembleUser(container)
        assembleDpNetwork(container)
    }
    
    private func assembleSystemServices(_ container: Container) {
        container.register(UIApplication.self) { _ in
            UIApplication.shared
        }.inObjectScope(.container)
        container.register(UIPasteboard.self) { _ in
            UIPasteboard.general
        }.inObjectScope(.container)
        container.register(NotificationCenter.self) { _ in
            NotificationCenter.default
        }.inObjectScope(.container)
        container.register(UNUserNotificationCenter.self) { _ in
            UNUserNotificationCenter.current()
        }.inObjectScope(.container)
        container.register(FileManager.self) { _ in
            FileManager.default
        }.inObjectScope(.container)
    }
    
    private func assembleCustomServices(_ container: Container) {
        container.register(AppLanguage.self) { _ in
            AppLanguage.default
        }.inObjectScope(.container)
        container.register(AppSessionManager.self) { r in
            AppSessionManager(notificationCenter: r.resolve(NotificationCenter.self)!)
        }.inObjectScope(.container)
        container.register(AuthStateObserver.self) { r in
            AuthStateObserver(user: r.resolve(User.self)!,
                              cache: InMemoryCacheImpl.shared,
                              userSession: r.resolve(UserSession.self)!,
                              cookiesStorage: r.resolve(WebCookiesStorable.self)!)
        }.inObjectScope(.container)
//        container.register(BiometricAuthService.self) { _ in
//            BiometricAuthService()
//        }.inObjectScope(.container)
//        container.register(ImageLoader.self) { _ in
//            KingfisherImageLoader()
//        }.inObjectScope(.container)
        container.register(PropertyFormatter.self) { r in
            PropertyFormatter(appLanguage: r.resolve(AppLanguage.self)!)
        }.inObjectScope(.container)
        container.register(Throttler.self) { _ in
            Throttler()
        }.inObjectScope(.transient)
//        container.register(WebViewController.self) { r, title, url in
//            WebViewController(title: title, url: url, urlResolver: r.resolve(UrlResolver.self)!, cookiesStorage: r.resolve(WebCookiesStorable.self)!)
//        }.inObjectScope(.transient)
//        container.register(WebCookiesStorable.self) { r in
//            WebCookiesStorage(user: r.resolve(User.self)!, userSession: r.resolve(UserSession.self)!)
//        }.inObjectScope(.container)
//        container.register(WebLinksFactory.self) { _ in
//            WebLinksFactory()
//        }.inObjectScope(.container)
        container.register(UrlResolver.self) { _ in
            RegexedUrlResolver(baseUrl: Environment.webBaseUrl)
        }.inObjectScope(.transient)
    }
    
    
    private func assembleUser(_ container: Container) {
        container.register(UserSessionStorage.self) { _ in
            UserSessionStorage()
        }.inObjectScope(.container)

        container.register(UserSession.self) { r in
            UserSession(storage: r.resolve(UserSessionStorage.self)!)
        }.inObjectScope(.container)

        container.register(User.self) { _ in
            User()
        }.inObjectScope(.container)

        container.register(UserRepository.self) { r in
            UserRepository(user: r.resolve(User.self)!, userSessionStorage: r.resolve(UserSessionStorage.self)!)
        }.initCompleted { r, userReposiotory in
            userReposiotory.setProvider(r.resolve(Provider.self)!)
        }.inObjectScope(.container)
    }
    
    private func assembleDpNetwork(_ container: Container) {
        container.register(AlamofireNetwork<ProviderError>.self, name: DependencyContextNames.dp.rawValue) { r in
            AlamofireNetwork<ProviderError>(baseUrl: Environment.apiBaseUrl,
                             auth: r.resolve(NetworkAuth.self)!,
                             headersStorage: r.resolve(NetworkHeadersStorable.self)!,
                             loggerLevel: Environment.isDebug ? .debug : .off)
        }.initCompleted { resolver, rest in
            rest.setAuthProvider(resolver.resolve(Provider.self)!)
        }.inObjectScope(.container)
        container.register(NetworkRequestBuilder.self, name: DependencyContextNames.dp.rawValue) { r in
            NetworkRequestBuilder(with: Environment.apiBaseUrl,
                                  injectedParameters: r.resolve(NetworkParametersStorable.self)!,
                                  parametersInjectionPolicy: .include)
        }.inObjectScope(.transient)
        container.register(NetworkAuth.self) { r in
            NetworkAuth(storage: r.resolve(UserSessionStorage.self)!,
                        observer: r.resolve(AuthStateObserver.self)!)
        }.inObjectScope(.container)
        container.register(NetworkHeadersStorable.self) { _ in
            NetworkHeadersStorage()
        }.inObjectScope(.container)
        container.register(NetworkParametersStorable.self) { r in
            NetworkParametersStorage(user: r.resolve(User.self)!)
        }
        container.register(Provider.self) { r in
            Provider(rest: r.resolve(AlamofireNetwork<ProviderError>.self, name: DependencyContextNames.dp.rawValue)!,
                     basicAuth: Environment.basicAuth,
                     builder: r.resolve(NetworkRequestBuilder.self, name: DependencyContextNames.dp.rawValue)!)
        }.inObjectScope(.container)
    }
}

