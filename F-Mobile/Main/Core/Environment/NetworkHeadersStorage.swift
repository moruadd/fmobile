//
//  NetworkHeadersStorage.swift
//  F-Mobile
//
//  Created by Damir on 30.10.2023.
//

import AlabsNetwork
import Fundamental
import UIKit

typealias Parameters = [String: Any]

struct NetworkHeadersStorage: NetworkHeadersStorable {
    var defaultHeaders: NetworkHeaders {
        var headers = [
            "operatorType": "TELE2",
            "clientType": "MOBILE",
            "Accept-Language": AppLanguage.current.rawValue,
            "platform": "ios",
            "appVersion": Environment.appVersion ?? "",
            "X-Image-Multiplier": Device.current.assetScale
        ]
        if let uuid = UIDevice.current.identifierForVendor?.uuidString {
            headers["deviceId"] = uuid
        }
        return headers
    }
}

struct NetworkParametersStorage: NetworkParametersStorable {
    private let user: User
    
    var parameters: Parameters {
        guard let msisdn = user.msisdn, !msisdn.isEmpty else { return [:] }
        return ["msisdn": msisdn]
    }
    
    init(user: User) {
        self.user = user
    }
}
