//
//  Environment.swift
//  F-Mobile
//
//  Created by Damir on 30.10.2023.
//

import UIKit

enum Environment {
    static let isDebug: Bool = {
        #if DEVELOPMENT || STAGE
            return true
        #else
            return false
        #endif
    }()
    
    static let bundleIdentifier = Bundle.main.bundleIdentifier ?? ""
    static let appSettingsURL: String = UIApplication.openSettingsURLString + Environment.bundleIdentifier
    static let appVersion: String? = { Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String }()
    
    // MARK: - Development
    #if DEVELOPMENT
        static var tusUploadUrl: String = "https://dev.tele2.kz/shop/api/assets/uploads"
        static var webBaseUrl: String = "https://dev.tele2.kz"
        static var apiBaseUrl: String = "https://dev.tele2.kz/apigw/v1"
        static var basicAuth: String = "Basic Y3VzdG9tLWNsaWVudDo="
        static var googleServiceFileName: String = "GoogleService-Info-Alpha"
        static var veriLiveUrl: String = "https://beta.tele2.kz/verilive/verilive"
        static let appsFlyerDevKey = "Ku8JkZEF9RJYwd4TQBXijm"
        static let appleAppID = "1633587397"
        static let amplitudeAPIKey = "014ea6c2374d4bb95010b0cb0083a2b8"
        static var appsFlyerOneLinkCustomDomains = ["click.tele2.kz"]
    // MARK: - Stage
    #elseif STAGE
        static var tusUploadUrl: String = "https://dev.tele2.kz/shop/api/assets/uploads"
        static var webBaseUrl: String = "https://dev.tele2.kz"
        static var apiBaseUrl: String = "https://dev.tele2.kz/apigw/v1"
        static var basicAuth: String = "Basic Y3VzdG9tLWNsaWVudDo="
        static var googleServiceFileName: String = "GoogleService-Info-Alpha"
        static var veriLiveUrl: String = "https://beta.tele2.kz/verilive/verilive"
        static let appsFlyerDevKey = "Ku8JkZEF9RJYwd4TQBXijm"
        static let appleAppID = "1633587397"
        static let amplitudeAPIKey = "014ea6c2374d4bb95010b0cb0083a2b8"
        static var appsFlyerOneLinkCustomDomains = ["click.tele2.kz"]
    // MARK: - Production
    #else
        static var tusUploadUrl: String = "https://tele2.kz/shop/api/assets/uploads"
        static var webBaseUrl: String = "https://dev.tele2.kz"
        static var apiBaseUrl: String = "https://dev.tele2.kz/mvno/v1"
        static var basicAuth: String = "Basic SU9TOg=="
        static var googleServiceFileName: String = "GoogleService-Info-Production"
        static var veriLiveUrl: String = "https://beta.tele2.kz/verilive/verilive"
        static let appsFlyerDevKey = "Ku8JkZEF9RJYwd4TQBXijm"
        static let appleAppID = "1541641406"
        static let amplitudeAPIKey = "56d3abd6fb0507cbd64f547efccf7d08"
        static var appsFlyerOneLinkCustomDomains = ["click.tele2.kz"]
    #endif
}

// swiftlint:disable prefixed_toplevel_constant
private let infoPlist: [String: Any] = {
    guard let infoPlist = Bundle.main.infoDictionary else {
        fatalError("Файл `Info.plist` не найден")
    }
    return infoPlist
}()

