//
//  DependencyModuleFactory.swift
//  F-Mobile
//
//  Created by Damir on 16.10.2023.
//

class DependencyModuleFactory {
    let container: DependencyContainer
    
    init(container: DependencyContainer) {
        self.container = container
    }
}

