//
//  AppDelegate.swift
//  F-Mobile
//
//  Created by Damir on 23.09.2023.
//

import UIKit
import Swinject

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    static let assembler = Assembler([DependencyContainerAssembly()])
    
    var window: UIWindow?
    private let configurator = AppDelegateConfiguratorFactory.makeDefault()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
//        setupWindow()
//        UNUserNotificationCenter.current().delegate = self
//        return configurator.application?(application, didFinishLaunchingWithOptions: launchOptions) ?? true
        let rootController = CoordinatorNavigationController(backBarButtonImage: ImageType.backUi.image, closeBarButtonImage: ImageType.closeLine.image)
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        window?.rootViewController = rootController
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        configurator.applicationDidBecomeActive?(application)
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
//        let userInfo = ["url": url]
//        NotificationCenter.default.post(name: .didReceiveDeepLink, object: nil, userInfo: userInfo)
        return true
    }
    
    func application(_ application: UIApplication,
                     continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
//        if userActivity.activityType == NSUserActivityTypeBrowsingWeb {
//            let userInfo: [String: Any] = ["url": userActivity.webpageURL as Any, "userActivity": userActivity]
//            NotificationCenter.default.post(name: .didReceiveDeepLink, object: nil, userInfo: userInfo as [AnyHashable: Any])
//            return true
//        }
        return false
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        let deviceToken = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
//        let userInfo = ["deviceToken": deviceToken]
//        NotificationCenter.default.post(name: .didReceivePushDeviceToken, object: nil, userInfo: userInfo)
    }
}

// MARK: - UNUserNotificationCenterDelegate

extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
//        let userInfo = response.notification.request.content.userInfo
//        if let payload = userInfo["payload"] as? [String: Any],
//           let categoryString = payload["categoryId"] as? String,
//           let categoryId = Int(categoryString) {
//            let userInfo = ["categoryId": categoryId]
//            NotificationCenter.default.post(name: .didReceivePushMessage, object: nil, userInfo: userInfo)
//        }
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
}

// MARK: - Private

private extension AppDelegate {
    
    func setupWindow() {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        window?.rootViewController = CoordinatorNavigationController(backBarButtonImage: ImageType.backUi.image, closeBarButtonImage: ImageType.closeLine.image)
    }
}
